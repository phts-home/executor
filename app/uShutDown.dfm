object Form_exwin: TForm_exwin
  Left = 188
  Top = 123
  BorderStyle = bsNone
  Caption = 'Form_exwin'
  ClientHeight = 104
  ClientWidth = 354
  Color = clMaroon
  TransparentColor = True
  TransparentColorValue = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -53
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 64
  object Label_time: TLabel
    Left = 174
    Top = 34
    Width = 68
    Height = 57
    AutoSize = False
    Caption = '60'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -53
    Font.Name = 'a_DexterOtl'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label_time02: TLabel
    Left = 87
    Top = 34
    Width = 87
    Height = 57
    Caption = '00:'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -53
    Font.Name = 'a_DexterOtl'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label1: TLabel
    Left = 15
    Top = 11
    Width = 321
    Height = 23
    Alignment = taCenter
    AutoSize = False
    Caption = #1055#1045#1056#1045#1047#1040#1043#1056#1059#1047#1050#1040' '#1050#1054#1052#1055#1068#1070#1058#1045#1056#1040
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -21
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 10
    Top = 65
    Width = 71
    Height = 31
    BevelOuter = bvNone
    Color = clMaroon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Shape1: TShape
      Left = 5
      Top = 0
      Width = 66
      Height = 25
      Brush.Color = clGreen
      Pen.Style = psClear
      Shape = stEllipse
    end
    object Label2: TLabel
      Left = 21
      Top = 5
      Width = 33
      Height = 13
      Cursor = crHandPoint
      Caption = '   X   '
      Color = clGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      OnClick = Label2Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 247
    Top = 56
  end
end
