unit uOptions;      //���������

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Spin, ExtCtrls, FileCtrl, ComCtrls, Grids,
  {Outline,} DirOutln, ImgList, MPlayer, IniFiles, Registry;

type
  TForm_opt = class(TForm)
    GroupBox1: TGroupBox;
    Edit_bud: TEdit;
    OpenDialog_bud: TOpenDialog;
    SpeedButton_bud: TSpeedButton;
    GroupBox2: TGroupBox;
    Edit_soob: TEdit;
    Edit_soob_mus: TEdit;
    SpeedButton_soob: TSpeedButton;
    CheckBox_mus: TCheckBox;
    OpenDialog_soob: TOpenDialog;
    Button_close: TButton;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    SpinEdit_timer: TSpinEdit;
    Label2: TLabel;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Edit_dir: TEdit;
    SpeedButton_file: TSpeedButton;
    CheckBox_timer: TCheckBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    MediaPlayer1: TMediaPlayer;
    ImageList1: TImageList;
    Timer1: TTimer;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    FileListBox2: TFileListBox;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    ListBox_temp: TListBox;
    GroupBox7: TGroupBox;
    CheckBox1: TCheckBox;
    Edit1: TEdit;
    SpeedButton6: TSpeedButton;
    OpenDialog_open: TOpenDialog;
    MediaPlayer2: TMediaPlayer;
    Timer2: TTimer;
    Button1: TButton;
    FileListBox1: TFileListBox;
    SpeedButton7: TSpeedButton;
    ListBox_temp2: TListBox;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    GroupBox8: TGroupBox;
    SpeedButton10: TSpeedButton;
    Label5: TLabel;
    Label6: TLabel;
    SpeedButton11: TSpeedButton;
    procedure CheckBox_musClick(Sender: TObject);
    procedure SpeedButton_budClick(Sender: TObject);
    procedure SpeedButton_soobClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpinEdit_timerMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton_fileClick(Sender: TObject);
    procedure Edit_dirChange(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure CheckBox_timerClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);

  private
    { Private declarations }
  public
    DisColor,EnColor: Integer;
    CurrentLanguage,                                   //������� ���� (����)
    CurrentScheme,                                     //������� ����� (����)
    StrName1,StrName2,
    StrVer,
    StrAuthor                    : String;
    IniLanguage, IniScheme, ini1: TIniFile;
    AssociationEx: Boolean;
    function ChangeLanguage(LangFile : string):Boolean;
    function ChangeScheme (SchemeFile : string):Boolean;
    procedure CreateLanguage;
    procedure CreateScheme;
    { Public declarations }
  end;

var
  Form_opt: TForm_opt;
  playing1,playing2:boolean;


implementation

uses uMain, uFolder, uRadioButtons, uTimer, uEdit, uAbout, uStopButton,
  uHelp, uShutDown;

{$R *.dfm}

procedure TForm_opt.CreateLanguage;
begin
CreateDir(extractfilepath(paramstr(0))+'Data\');
Form_opt.ListBox_temp.Items.SaveToFile(extractfilepath(paramstr(0))+'Data\rus.lng');
end;

procedure TForm_opt.CreateScheme;
begin
CreateDir(extractfilepath(paramstr(0))+'Data\');
Form_opt.ListBox_temp2.Items.SaveToFile(extractfilepath(paramstr(0))+'Data\standart.col');
end;

procedure TForm_opt.CheckBox_musClick(Sender: TObject);
begin
if checkbox_mus.Checked=false
  then
    begin
    edit_soob_mus.Enabled:=false;
    edit_soob_mus.Color:=DisColor;
    end
  else
    begin
    edit_soob_mus.Enabled:=true;
    edit_soob_mus.Color:=EnColor;
    end;
end;

procedure TForm_opt.SpeedButton_budClick(Sender: TObject);
var im: TBitMap;
begin
im:=tbitmap.Create;
mediaplayer1.Close;
imagelist1.GetBitmap(1,im);
speedbutton1.Glyph:=im;
timer1.Enabled:=false;
playing1:=false;
if FileExists(edit_bud.Text)
  then opendialog_bud.FileName:= edit_bud.Text
  else
    begin
    opendialog_bud.FileName:='';
    opendialog_bud.InitialDir:=extractfilepath(paramstr(0));
    end;
if opendialog_bud.Execute then edit_bud.Text:=opendialog_bud.FileName;
end;

procedure TForm_opt.SpeedButton_soobClick(Sender: TObject);
var im:TBitMap;
begin
im:=tbitmap.Create;
mediaplayer2.Close;
imagelist1.GetBitmap(1,im);
speedbutton2.Glyph:=im;
timer2.Enabled:=false;
playing2:=false;

if FileExists(Edit_soob_mus.Text)
  then opendialog_soob.FileName:= Edit_soob_mus.Text
  else
    begin
    opendialog_soob.FileName:='';
    opendialog_soob.InitialDir:=extractfilepath(paramstr(0));
    end;
if opendialog_soob.Execute then edit_soob_mus.Text:=opendialog_soob.FileName;
end;

procedure TForm_opt.FormCreate(Sender: TObject);
var im:TBitMap;
begin
im:=tbitmap.Create;
imagelist1.GetBitmap(1,im);
speedbutton1.Glyph:=im;
speedbutton2.Glyph:=im;
end;

procedure TForm_opt.FormClose(Sender: TObject; var Action: TCloseAction);
var im:tbitmap;
begin
if Edit1.text=''
  then CheckBox1.Checked:=false;
if Edit_soob_mus.text=''
  then CheckBox_mus.Checked:=false;

Form_main.Ini.WriteString('options','music',edit_bud.Text);
Form_main.Ini.WriteString('options','message',edit_soob.Text);
Form_main.Ini.WriteString('options','mmusic',edit_soob_mus.Text);
Form_main.Ini.WriteString('options','directory',edit_dir.Text);
Form_main.Ini.WriteInteger('options','timer',spinedit_timer.Value);
Form_main.Ini.WriteBool('options','checkmusic',checkbox_mus.Checked);
Form_main.Ini.WriteBool('options','checktimer',checkbox_timer.Checked);
Form_main.Ini.WriteBool('options','autoload',checkbox1.Checked);
Form_main.Ini.WriteString('options','autofile',edit1.Text);

Form_main.Ini.WriteString('options','language',CurrentLanguage);
im:=tbitmap.Create;
mediaplayer1.Close;
mediaplayer2.Close;
imagelist1.GetBitmap(1,im);
speedbutton1.Glyph:=im;
speedbutton2.Glyph:=im;
timer1.Enabled:=false;
timer2.Enabled:=false;
playing1:=false;
playing2:=false;
end;

procedure TForm_opt.FormShow(Sender: TObject);
var Reg: TRegistry;
begin
edit_bud.SelStart:=0;
edit_soob.SelStart:=0;
edit_soob_mus.SelStart:=0;
edit_dir.SelStart:=0;
edit1.SelStart:=0;
Form_opt.Left:=Form_main.Left+20;
Form_opt.Top:=Form_main.top+10;
button_close.SetFocus;
try
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  finally
    Reg.OpenKey('xxcfile\shell\open\command', True);
    if Reg.ReadString('')=Application.ExeName+' "%1"' then
      begin
      Label6.Caption:=IniLanguage.ReadString('options','624','');
      AssociationEx:=True;
      end else
      begin
      Label6.Caption:=IniLanguage.ReadString('options','625','');
      AssociationEx:=False;
      end;
  end;
Reg.Free;
end;

procedure TForm_opt.SpinEdit_timerMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if shift=[ssmiddle] then spinedit_timer.value:=spinedit_timer.MinValue;
end;

procedure TForm_opt.SpeedButton_fileClick(Sender: TObject);
begin
if not DirectoryExists(edit_dir.Text)
  then
  begin
  if DirectoryExists(extractfilepath(paramstr(0))+'Lists\')
    then edit_dir.Text:=extractfilepath(paramstr(0))+'Lists\'
    else edit_dir.Text:=extractfilepath(paramstr(0));
  end;
Form_dir.shellTreeView1.Path:=Edit_dir.text;
form_dir.showmodal;
end;

procedure TForm_opt.Edit_dirChange(Sender: TObject);
begin
if edit_dir.Text<>' '
  then edit_dir.MaxLength:=0
  else edit_dir.MaxLength:=1;
end;

procedure TForm_opt.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
if spinedit_timer.Focused
  then
    begin
    if WheelDelta=120
      then spinedit_timer.value:=spinedit_timer.value+1;
    if WheelDelta=-120
      then spinedit_timer.value:=spinedit_timer.value-1;
    end;
end;


procedure TForm_opt.CheckBox_timerClick(Sender: TObject);
begin
if checkbox_timer.Checked=false
  then
    begin
    SpinEdit_timer.Enabled:=false;
    SpinEdit_timer.Color:=DisColor;
    end
  else
    begin
    SpinEdit_timer.Enabled:=true;
    SpinEdit_timer.Color:=EnColor;
    end;
end;

procedure TForm_opt.SpeedButton1Click(Sender: TObject);
var im:tbitmap;
begin
im:=tbitmap.Create;
if playing1
  then
  begin
  mediaplayer1.Close;
  imagelist1.GetBitmap(1,im);
  speedbutton1.Glyph:=im;
  playing1:=false;
  timer1.Enabled:=false;
  end
  else
  begin
  try
    with mediaplayer1 do
    begin
    Close;
    filename:=edit_bud.Text;
    open;
    play;
    end;
    except end;
  imagelist1.GetBitmap(0,im);
  speedbutton1.Glyph:=im;
  playing1:=true;
  timer1.Enabled:=true;

  im:=tbitmap.Create;
  mediaplayer2.Close;
  imagelist1.GetBitmap(1,im);
  speedbutton2.Glyph:=im;
  playing2:=false;
  timer2.Enabled:=false;
  end;
end;

procedure TForm_opt.SpeedButton2Click(Sender: TObject);
var im:tbitmap;
begin
im:=tbitmap.Create;
if playing2
  then
  begin
  mediaplayer2.Close;
  imagelist1.GetBitmap(1,im);
  speedbutton2.Glyph:=im;
  playing2:=false;
  timer2.Enabled:=false;
  end
  else
  begin
  mediaplayer1.Close;
  imagelist1.GetBitmap(1,im);
  speedbutton1.Glyph:=im;
  playing1:=false;
  timer1.Enabled:=false;

  im:=tbitmap.Create;
  try
    with mediaplayer2 do
    begin
    Close;
    filename:=edit_soob_mus.Text;
    open;
    play;
    end;
    except end;
  imagelist1.GetBitmap(0,im);
  speedbutton2.Glyph:=im;
  playing2:=true;
  timer2.Enabled:=true;
  end;
end;

procedure TForm_opt.Timer1Timer(Sender: TObject);
var im:tbitmap;
begin
if mediaplayer1.Position=mediaplayer1.Length
  then
  begin
  im:=tbitmap.Create;
  imagelist1.GetBitmap(1,im);
  speedbutton1.Glyph:=im;
  timer2.Enabled:=false;
  playing1:=false;
  Timer1.Enabled:=false;
  end;
end;

procedure TForm_opt.Timer2Timer(Sender: TObject);
var im:tbitmap;
begin
if mediaplayer2.Position=mediaplayer2.Length
  then
  begin
  im:=tbitmap.Create;
  imagelist1.GetBitmap(1,im);
  speedbutton2.Glyph:=im;
  timer2.Enabled:=false;
  playing2:=false;
  Timer1.Enabled:=false;
  end;
end;

function TForm_opt.ChangeLanguage(LangFile: string): boolean;
var t:textfile;
    st:string;
    i,p:integer;
begin
if fileExists(LangFile) then
  begin
  ChangeLanguage:=true;
  assignfile(t,LangFile);
  IniLanguage:=TIniFile.Create(LangFile);
  if IniLanguage.ReadString('main','ver','')<>'2.1'
    then
    begin
    ChangeLanguage:=false;
    Exit;
    end
    else
    begin
    if IniLanguage.ReadString('main','lang','')='�������'
      then
      begin
      Form_help.Label9.Caption:='������ �����';
      end
      else
      begin
      Form_help.Label9.Caption:='Phil Tsarik';
      end;
    CurrentLanguage:=LangFile;
    end;
  Form_main.N1.Caption:=IniLanguage.ReadString('Menu','1','');
  Form_main.new.Caption:=IniLanguage.ReadString('Menu','2','');
  Form_main.open.Caption:=IniLanguage.ReadString('Menu','3','');
  Form_main.save.Caption:=IniLanguage.ReadString('Menu','4','');
  Form_main.N35.Caption:=IniLanguage.ReadString('Menu','5','');
  Form_main.N33.Caption:=IniLanguage.ReadString('Menu','6','');
  Form_main.N32.Caption:=IniLanguage.ReadString('Menu','7','');
  Form_main.print.Caption:=IniLanguage.ReadString('Menu','8','');
  Form_main.PrintAll.Caption:=IniLanguage.ReadString('Menu','9','');
  Form_main.exit.Caption:=IniLanguage.ReadString('Menu','10','');
  Form_main.N2.Caption:=IniLanguage.ReadString('Menu','11','');
  Form_main.current.Caption:=IniLanguage.ReadString('Menu','12','');
  Form_main.N14.Caption:=IniLanguage.ReadString('Menu','13','');
  Form_main.N15.Caption:=IniLanguage.ReadString('Menu','14','');
  Form_main.N16.Caption:=IniLanguage.ReadString('Menu','15','');
  Form_main.N17.Caption:=IniLanguage.ReadString('Menu','16','');
  Form_main.N18.Caption:=IniLanguage.ReadString('Menu','17','');
  Form_main.N20.Caption:=IniLanguage.ReadString('Menu','18','');
  Form_main.opt.Caption:=IniLanguage.ReadString('Menu','19','');
  Form_main.N3.Caption:=IniLanguage.ReadString('Menu','20','');
  Form_main.add.Caption:=IniLanguage.ReadString('Menu','21','');
  Form_main.bud.Caption:=IniLanguage.ReadString('Menu','22','');
  Form_main.soob.Caption:=IniLanguage.ReadString('Menu','23','');
  Form_main.reboot.Caption:=IniLanguage.ReadString('Menu','24','');
  Form_main.exwin.Caption:=IniLanguage.ReadString('Menu','25','');
  Form_main.N51.Caption:=IniLanguage.ReadString('Menu','26','');
  Form_main.N110.Caption:=IniLanguage.ReadString('Menu','27','');
  Form_main.N52.Caption:=IniLanguage.ReadString('Menu','28','');
  Form_main.N111.Caption:=IniLanguage.ReadString('Menu','29','');
  Form_main.N31.Caption:=IniLanguage.ReadString('Menu','30','');
  Form_main.N7.Caption:=IniLanguage.ReadString('Menu','31','');
  Form_main.N11.Caption:=IniLanguage.ReadString('Menu','32','');
  Form_main.PrintTimer.Caption:=IniLanguage.ReadString('Menu','33','');
  Form_main.N19.Caption:=IniLanguage.ReadString('Menu','34','');
  Form_main.N9.Caption:=IniLanguage.ReadString('Menu','35','');
  Form_main.N12.Caption:=IniLanguage.ReadString('Menu','36','');
  Form_main.N13.Caption:=IniLanguage.ReadString('Menu','37','');
  Form_main.N37.Caption:=IniLanguage.ReadString('Menu','38','');
  Form_main.N114.Caption:=IniLanguage.ReadString('Menu','39','');
  Form_main.N113.Caption:=IniLanguage.ReadString('Menu','40','');
  Form_main.N112.Caption:=IniLanguage.ReadString('Menu','41','');
  Form_main.N4.Caption:=IniLanguage.ReadString('Menu','42','');
  Form_main.N29.Caption:=IniLanguage.ReadString('Menu','43','');
  Form_main.abprog.Caption:=IniLanguage.ReadString('Menu','44','');

  form_max1.RadioButton_file.Caption:=IniLanguage.ReadString('radiobutton','101','');
  form_max2.RadioButton_file.Caption:=IniLanguage.ReadString('radiobutton','101','');
  form_max1.GroupBox1.Caption:=' '+IniLanguage.ReadString('radiobutton','102','')+' ';
  form_max2.GroupBox1.Caption:=' '+IniLanguage.ReadString('radiobutton','102','')+' ';
  form_max1.RadioButton_go.Caption:=IniLanguage.ReadString('radiobutton','103','');
  form_max2.RadioButton_go.Caption:=IniLanguage.ReadString('radiobutton','103','');
  form_edit.RadioButton_go.Caption:=IniLanguage.ReadString('radiobutton','103','');
  form_max1.RadioButton_kill.Caption:=IniLanguage.ReadString('radiobutton','104','');
  form_max2.RadioButton_kill.Caption:=IniLanguage.ReadString('radiobutton','104','');
  form_edit.RadioButton_kill.Caption:=IniLanguage.ReadString('radiobutton','104','');
  form_max1.GroupBox2.Caption:=' '+IniLanguage.ReadString('radiobutton','105','')+' ';
  form_max2.RadioGroup1.Caption:=' '+IniLanguage.ReadString('radiobutton','105','')+' ';
  form_edit.RadioGroup1.Caption:=' '+IniLanguage.ReadString('radiobutton','105','')+' ';
  form_max1.RadioButton_capt.Caption:=IniLanguage.ReadString('radiobutton','106','');
  form_max2.RadioButton_capt.Caption:=IniLanguage.ReadString('radiobutton','106','');
  form_edit.RadioButton_capt.Caption:=IniLanguage.ReadString('radiobutton','106','');
  form_max1.RadioButton_class.Caption:=IniLanguage.ReadString('radiobutton','107','');
  form_max2.RadioButton_class.Caption:=IniLanguage.ReadString('radiobutton','107','');
  form_edit.RadioButton_class.Caption:=IniLanguage.ReadString('radiobutton','107','');

  Form_main.BitBtn1.Caption:=IniLanguage.ReadString('buttons','201','');
  Form_main.BitBtn2.Caption:=IniLanguage.ReadString('buttons','202','');
  Form_main.BitBtn3.Caption:=IniLanguage.ReadString('buttons','203','');
  Form_main.BitBtn5.Caption:=IniLanguage.ReadString('buttons','204','');
  form_max2.BitBtn1.Caption:=IniLanguage.ReadString('buttons','205','');
  form_max2.BitBtn2.Caption:=IniLanguage.ReadString('buttons','206','');
  form_max2.BitBtn3.Caption:=IniLanguage.ReadString('buttons','207','');
  form_max2.ButPause:=IniLanguage.ReadString('buttons','207','');
  form_max2.ButCon:=IniLanguage.ReadString('buttons','208','');
  form_max2.BitBtn4.Caption:=IniLanguage.ReadString('buttons','209','');

  Form_main.tray_listbox1:=IniLanguage.ReadString('statusbar','301','');
  Form_main.tray_button1:=IniLanguage.ReadString('statusbar','302','');
  Form_main.tray_button2:=IniLanguage.ReadString('statusbar','303','');
  Form_main.tray_button3:=IniLanguage.ReadString('statusbar','304','');
  Form_main.tray_button4:=IniLanguage.ReadString('statusbar','305','');
  Form_main.tray_button5:=IniLanguage.ReadString('statusbar','306','');
  form_max1.tray_button6:=IniLanguage.ReadString('statusbar','307','');
  form_max2.tray_button7:=IniLanguage.ReadString('statusbar','308','');
  form_max2.tray_button8:=IniLanguage.ReadString('statusbar','309','');
  form_max2.tray_button9:=IniLanguage.ReadString('statusbar','310','');
  form_max2.tray_button10:=IniLanguage.ReadString('statusbar','311','');

  form_help.Caption:=IniLanguage.ReadString('about','401','');
  form_help.Label2.Caption:=IniLanguage.ReadString('about','402','');
  form_help.Label3.Caption:=IniLanguage.ReadString('about','403','');
  form_help.Label11.Caption:=IniLanguage.ReadString('about','404','');
  form_help.Button_close.Caption:=IniLanguage.ReadString('about','405','');

  form_edit.Caption:=IniLanguage.ReadString('edit','501','');
  form_edit.Button_save.Caption:=IniLanguage.ReadString('edit','502','');
  form_edit.Button_cancel.Caption:=IniLanguage.ReadString('edit','503','');
  form_edit.Button_add.Caption:=IniLanguage.ReadString('edit','504','');
  form_edit.SpeedButton1.Caption:=IniLanguage.ReadString('edit','505','');
  form_edit.SpeedButton2.Caption:=IniLanguage.ReadString('edit','506','');
  form_edit.SpeedButton3.Caption:=IniLanguage.ReadString('edit','507','');
  form_edit.SpeedButton4.Caption:=IniLanguage.ReadString('edit','508','');

  form_opt.Caption:=IniLanguage.ReadString('options','601','');
  GroupBox1.Caption:=' '+IniLanguage.ReadString('options','602','')+' ';
  GroupBox2.Caption:=' '+IniLanguage.ReadString('options','603','')+' ';
  CheckBox_mus.Caption:=IniLanguage.ReadString('options','604','');
  GroupBox3.Caption:=' '+IniLanguage.ReadString('options','605','')+' ';
  Label1.Caption:=IniLanguage.ReadString('options','606','');
  Label2.Caption:=IniLanguage.ReadString('options','607','');
  CheckBox_timer.Caption:=IniLanguage.ReadString('options','608','');
  GroupBox4.Caption:=' '+IniLanguage.ReadString('options','609','')+' ';
  Label3.Caption:=IniLanguage.ReadString('options','610','');
  Label4.Caption:=IniLanguage.ReadString('options','611','');
  GroupBox5.Caption:=' '+IniLanguage.ReadString('options','612','')+' ';
  GroupBox6.Caption:=' '+IniLanguage.ReadString('options','613','')+' ';
  GroupBox7.Caption:=' '+IniLanguage.ReadString('options','614','')+' ';
  CheckBox1.Caption:=IniLanguage.ReadString('options','615','');
  button_close.Caption:=IniLanguage.ReadString('options','616','');
  button1.Caption:=IniLanguage.ReadString('options','617','');
  GroupBox8.Caption:=' '+IniLanguage.ReadString('options','621','')+' ';
  Label5.Caption:=IniLanguage.ReadString('options','623','');
  Form_main.ErrorMessage7:=IniLanguage.ReadString('options','622','');
  if AssociationEx then
    begin
    Label6.Caption:=IniLanguage.ReadString('options','624','');
    end else
    begin
    Label6.Caption:=IniLanguage.ReadString('options','625','');
    end;
  form_dir.Caption:=IniLanguage.ReadString('options','618','');
  form_dir.Button_close.Caption:=IniLanguage.ReadString('options','619','');
  StrName2:=IniLanguage.ReadString('options','613','');
  StrName1:=IniLanguage.ReadString('options','612','');
  StrVer:=Inilanguage.ReadString('options','620','');
  StrAuthor:=Inilanguage.ReadString('about','403','');

  Form_main.ErrorMessage1:=IniLanguage.ReadString('messages','701','');
  Form_main.ErrorMessage2:=IniLanguage.ReadString('messages','702','');
  Form_main.ErrorMessage3:=IniLanguage.ReadString('messages','703','');
//  Form_main.ErrorMessage4:=IniLanguage.ReadString('messages','704','');
  Form_main.ErrorMessage5:=IniLanguage.ReadString('messages','704','');
  Form_main.ErrorMessage6:=IniLanguage.ReadString('messages','705','');
  Form_main.strPrn:=IniLanguage.ReadString('messages','706','');
  Form_main.strAlarm:='<< '+IniLanguage.ReadString('messages','707','')+' >>';
  Form_main.strMessage:='<| '+IniLanguage.ReadString('messages','708','')+' |>';
  Form_main.strReboot:='<* '+IniLanguage.ReadString('messages','709','')+' *>';
  Form_main.strShut:='<_ '+IniLanguage.ReadString('messages','710','')+' _>';

  Form_main.RestoreItem.Caption:=IniLanguage.ReadString('popup','801','');
  Form_main.N23.Caption:=IniLanguage.ReadString('popup','802','');
  Form_main.N25.Caption:=IniLanguage.ReadString('popup','803','');
  Form_main.N27.Caption:=IniLanguage.ReadString('popup','804','');
  Form_main.N24.Caption:=IniLanguage.ReadString('popup','805','');
  Form_main.N26.Caption:=IniLanguage.ReadString('popup','806','');
  Form_main.CloseItem.Caption:=IniLanguage.ReadString('popup','807','');

  form_play.Button1.Caption:=IniLanguage.ReadString('misc','901','');
  form_help2.Caption:=IniLanguage.ReadString('misc','902','');
  Form_main.strRebooting:=IniLanguage.ReadString('misc','903','');
  Form_main.strShuting:=' '+IniLanguage.ReadString('misc','904','')+' ';

{  reset(t);
  form_help2.Memo1.Clear;
  while not eof(t) do
    begin
    readln(t,s);
    if s='[help]'
      then
      begin
      while not eof(t) do
        begin
        readln(t,s);
        form_help2.Memo1.Lines.Add(s);
        end;
      end;
    end;
  closefile(t);  }
  for i:=0 to Form_main.ListBox_programs.Items.Count-1 do
    begin
    if pos('<<',Form_main.listbox_programs.Items[i])<>0 then
      begin
      p:=pos('<<',Form_main.listbox_programs.Items[i]);
      st:=Form_main.listbox_programs.Items[i];
      delete(st,p,length(Form_main.listbox_programs.Items[i]));
      st:=st+Form_main.strAlarm;
      Form_main.listbox_programs.Items[i]:=st;
      end;
    if pos('<|',Form_main.listbox_programs.Items[i])<>0 then
      begin
      p:=pos('<|',Form_main.listbox_programs.Items[i]);
      st:=Form_main.listbox_programs.Items[i];
      delete(st,p,length(Form_main.listbox_programs.Items[i]));
      st:=st+Form_main.strMessage;
      Form_main.listbox_programs.Items[i]:=st;
      end;
    if pos('<*',Form_main.listbox_programs.Items[i])<>0 then
      begin
      p:=pos('<*',Form_main.listbox_programs.Items[i]);
      st:=Form_main.listbox_programs.Items[i];
      delete(st,p,length(Form_main.listbox_programs.Items[i]));
      st:=st+Form_main.strReboot;
      Form_main.listbox_programs.Items[i]:=st;
      end;
    if pos('<_',Form_main.listbox_programs.Items[i])<>0 then
      begin
      p:=pos('<_',Form_main.listbox_programs.Items[i]);
      st:=Form_main.listbox_programs.Items[i];
      delete(st,p,length(Form_main.listbox_programs.Items[i]));
      st:=st+Form_main.strShut;
      Form_main.listbox_programs.Items[i]:=st;
      end;
    end;
  for i:=0 to form_max2.ListBox_prog.Items.Count-1 do
  begin
    if pos('<|',form_max2.ListBox_prog.Items[i])<>0 then
      begin
      p:=pos('<|',form_max2.ListBox_prog.Items[i]);
      st:=form_max2.ListBox_prog.Items[i];
      delete(st,p,length(form_max2.ListBox_prog.Items[i]));
      st:=st+Form_main.strMessage;
      form_max2.ListBox_prog.Items[i]:=st;
      end;
    if pos('<*',form_max2.ListBox_prog.Items[i])<>0 then
      begin
      p:=pos('<*',form_max2.ListBox_prog.Items[i]);
      st:=form_max2.ListBox_prog.Items[i];
      delete(st,p,length(form_max2.ListBox_prog.Items[i]));
      st:=st+Form_main.strReboot;
      form_max2.ListBox_prog.Items[i]:=st;
      end;
    if pos('<_',form_max2.ListBox_prog.Items[i])<>0 then
      begin
      p:=pos('<_',form_max2.ListBox_prog.Items[i]);
      st:=form_max2.ListBox_prog.Items[i];
      delete(st,p,length(form_max2.ListBox_prog.Items[i]));
      st:=st+Form_main.strShut;
      form_max2.ListBox_prog.Items[i]:=st;
      end;
    end;
  form_main.ComboBox_prog.Text:='';
  form_max2.ComboBox_prog.Text:='';
  end
  else ChangeLanguage:=false;
end;

function TForm_opt.ChangeScheme(SchemeFile: string): boolean;
var t:textfile;
//    bmp:TBitmap;
begin
if fileExists(SchemeFile) then
  begin
  assignfile(t,SchemeFile);
  IniLanguage:=TIniFile.Create(SchemeFile);
  if IniLanguage.ReadString('main','ver','')<>'2.1'
    then
    begin
    ChangeScheme:=false;
    Exit;
    end
    else
    begin
    CurrentScheme:=SchemeFile;
    end;
  ChangeScheme:=true;
  Form_main.Color:=IniLanguage.ReadInteger('main','form',$0000000);
  Form_main.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_main.listbox_programs.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_main.listbox_programs.Update;
  Form_main.MaskEdit_time.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_main.DateTimePicker_date.Color:=IniLanguage.ReadInteger('program','datepicker',$0000000);
    Form_main.DateTimePicker_date.CalColors.BackColor:=IniLanguage.ReadInteger('program','datepickerback',$0000000);
    Form_main.DateTimePicker_date.CalColors.MonthBackColor:=IniLanguage.ReadInteger('program','datepickermonthback',$0000000);
    Form_main.DateTimePicker_date.CalColors.TextColor:=IniLanguage.ReadInteger('program','datepickertext',$0000000);
    Form_main.DateTimePicker_date.CalColors.TitleBackColor:=IniLanguage.ReadInteger('program','datepickertitleback',$0000000);
    Form_main.DateTimePicker_date.CalColors.TitleTextColor:=IniLanguage.ReadInteger('program','datepickertetletext',$0000000);
    Form_main.DateTimePicker_date.CalColors.TrailingTextColor:=IniLanguage.ReadInteger('program','datepickertrailingtex',$0000000);
  Form_main.ComboBox_prog.Color:=IniLanguage.ReadInteger('main','window',$0000000);

  Form_opt.Color:=IniLanguage.ReadInteger('main','form',$0000000);
  Form_opt.Font.Color:=IniLanguage.ReadInteger('main','formtext',$0000000);
  Form_opt.Edit_bud.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.Edit_soob.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.Edit_dir.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.Edit1.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.Edit_soob_mus.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.SpinEdit_timer.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.EnColor:=IniLanguage.ReadInteger('options','enabled',$0000000);
  Form_opt.DisColor:=IniLanguage.ReadInteger('options','disenabled',$0000000);
  Form_opt.Edit_bud.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.Edit_soob.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.Edit_dir.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.Edit1.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.Edit_soob_mus.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.SpinEdit_timer.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.FileListBox1.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.FileListBox2.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_opt.FileListBox1.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.FileListBox1.Update;
  Form_opt.FileListBox2.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_opt.FileListBox2.Update;

  Form_edit.Color:=IniLanguage.ReadInteger('main','form',$0000000);
  Form_edit.Font.Color:=IniLanguage.ReadInteger('main','formtext',$0000000);
  Form_edit.Edit_programs.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_edit.DateTimePicker_date.Color:=IniLanguage.ReadInteger('program','datepicker',$0000000);
    Form_edit.DateTimePicker_date.CalColors.BackColor:=IniLanguage.ReadInteger('program','datepickerback',$0000000);
    Form_edit.DateTimePicker_date.CalColors.MonthBackColor:=IniLanguage.ReadInteger('program','datepickermonthback',$0000000);
    Form_edit.DateTimePicker_date.CalColors.TextColor:=IniLanguage.ReadInteger('program','datepickertext',$0000000);
    Form_edit.DateTimePicker_date.CalColors.TitleBackColor:=IniLanguage.ReadInteger('program','datepickertitleback',$0000000);
    Form_edit.DateTimePicker_date.CalColors.TitleTextColor:=IniLanguage.ReadInteger('program','datepickertetletext',$0000000);
    Form_edit.DateTimePicker_date.CalColors.TrailingTextColor:=IniLanguage.ReadInteger('program','datepickertrailingtex',$0000000);
  Form_edit.MaskEdit_time.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_edit.SpinEdit_val.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_edit.MaskEdit_time.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_edit.SpinEdit_val.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);
  Form_edit.Edit_programs.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);

  Form_help.Color:=IniLanguage.ReadInteger('about','form',$0000000);
  Form_help.Font.Color:=IniLanguage.ReadInteger('about','font',$0000000);
//  Bmp := TBitmap.Create;
//  Form_help.ImageList1.GetBitmap(IniLanguage.ReadInteger('about','pic',0),bmp);
//  Form_help.Image1.Canvas.Draw(0,0,bmp);

//  Form_help2.Color:=IniLanguage.ReadInteger('main','form',$0000000);
//  Form_help2.Memo1.Font.Color:=IniLanguage.ReadInteger('main','formtext',$0000000);

  Form_dir.Color:=IniLanguage.ReadInteger('main','form',$0000000);
  Form_dir.Font.Color:=IniLanguage.ReadInteger('main','font',$0000000);

  Form_play.Color:=IniLanguage.ReadInteger('main','form',$0000000);

  Form_max1.Color:=IniLanguage.ReadInteger('radiobuttons','form',$0000000);
  Form_max1.SpinEdit_val.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_max1.SpinEdit_val.Font.Color:=IniLanguage.ReadInteger('radiobuttons','font2',$0000000);
  Form_max1.Font.Color:=IniLanguage.ReadInteger('radiobuttons','font',$0000000);

  Form_max2.Color:=IniLanguage.ReadInteger('timer','form',$0000000);
  Form_max2.Font.Color:=IniLanguage.ReadInteger('timer','font',$0000000);
  Form_max2.MaskEdit.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_max2.MaskEdit.Font.Color:=IniLanguage.ReadInteger('timer','font2',$0000000);
  Form_max2.ComboBox_prog.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_max2.ComboBox_prog.Font.Color:=IniLanguage.ReadInteger('timer','font2',$0000000);
  Form_max2.ListBox_prog.Color:=IniLanguage.ReadInteger('main','window',$0000000);
  Form_max2.ListBox_prog.Font.Color:=IniLanguage.ReadInteger('timer','font2',$0000000);
  end
  else  ChangeScheme:=false;
end;

procedure TForm_opt.SpeedButton3Click(Sender: TObject);
begin
if Form_opt.ChangeLanguage(FileListBox2.FileName)=false
  then Application.MessageBox(pchar(Form_main.ErrorMessage5+#13+FileListBox2.FileName),pchar(Form_opt.Caption),mb_Ok+mb_iconerror);
FileListBox2.Update;
Button_close.SetFocus;
end;

procedure TForm_opt.SpeedButton4Click(Sender: TObject);
begin
if Form_opt.ChangeScheme(FileListBox1.FileName)=false
  then Application.MessageBox(pchar(Form_main.ErrorMessage5+#13+FileListBox1.FileName),pchar(Form_opt.Caption),mb_Ok+mb_iconerror)
  else
  begin
  if not Form_opt.CheckBox_mus.Checked
    then Form_opt.Edit_soob_mus.Color:=DisColor;
  if not Form_opt.CheckBox_timer.Checked
    then Form_opt.SpinEdit_timer.Color:=DisColor;
  if not Form_opt.CheckBox1.Checked
    then Form_opt.Edit1.Color:=DisColor;
  Button_close.SetFocus;
  Form_main.listbox_programs.Items.Add('');
  Form_main.listbox_programs.Items.Delete(0);
//  Close;
//  Form_main.opt.Click;
  end;
end;

procedure TForm_opt.SpeedButton5Click(Sender: TObject);
begin
fileListBox2.Update;
end;

procedure TForm_opt.SpeedButton6Click(Sender: TObject);
begin
if FileExists(Edit1.Text)
  then opendialog_open.FileName:= Edit1.Text
  else
    begin
    opendialog_open.FileName:='';
    opendialog_open.InitialDir:=Edit_dir.Text;
    end;
if opendialog_open.Execute then
  begin
  Edit1.text:=opendialog_open.filename;
  CheckBox1.Checked:=true;
  end;
end;

procedure TForm_opt.CheckBox1Click(Sender: TObject);
begin
if CheckBox1.Checked=false
  then
    begin
    edit1.Enabled:=false;
    edit1.Color:=DisColor;
    end
  else
    begin
    edit1.Enabled:=true;
    edit1.Color:=EnColor;
    end;
end;

procedure TForm_opt.Button1Click(Sender: TObject);
begin
if application.MessageBox(pchar(Form_main.ErrorMessage6),pchar(Form_main.caption),MB_ICONQUESTION+MB_YESNO)=6
  then
  begin
  Form_opt.Close;
  Form_opt.CreateLanguage;
  Form_opt.CreateScheme;
  Form_main.CreateExecutorData;
  Form_main.ReadIniFile(Form_main.CloserFile);
  end;
end;

procedure TForm_opt.SpeedButton7Click(Sender: TObject);
begin
fileListBox1.Update;
end;

procedure TForm_opt.SpeedButton8Click(Sender: TObject);
begin
if FileListBox1.FileName<>'' then
  begin
  Ini1:=TIniFile.Create(FileListBox1.FileName);
  application.MessageBox(pchar(StrName1+' '+Ini1.ReadString('main','name','')+#13+StrVer+': '+Ini1.ReadString('main','ver','')
                                          +#13+#13+StrAuthor+' '+Ini1.ReadString('main','author','')),pchar(form_opt.Caption),mb_ok+mb_iconinformation);
  end;
end;

procedure TForm_opt.SpeedButton9Click(Sender: TObject);
begin
if FileListBox2.FileName<>'' then
  begin
  Ini1:=TIniFile.Create(FileListBox2.FileName);
  application.MessageBox(pchar(StrName2+' '+Ini1.ReadString('main','lang','')+#13+StrVer+': '+Ini1.ReadString('main','ver','')
                                          +#13+#13+StrAuthor+' '+Ini1.ReadString('main','author','')),pchar(form_opt.Caption),mb_ok+mb_iconinformation);
  end;
end;

{procedure TForm_opt.Button2Click(Sender: TObject);
var Reg: TRegistry;
begin
try
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  finally
    if Application.MessageBox(PChar(Form_main.ErrorMessage7),PChar(Form_main.Caption), MB_YESNO+MB_ICONQUESTION) = idYes then
      begin
      Reg.OpenKey('.xxc', True);
      Reg.WriteString('', 'xxcfile');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\DefaultIcon', True);
      Reg.WriteString('', extractfilepath(paramstr(0))+'FileIcon.ico');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\shell\open\command', True);
      Reg.WriteString('', Application.ExeName+' "%1"');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\', True);
      Reg.WriteString('', 'Executor 2.1 Task List');
      Reg.CloseKey;
      end;
  end;
Reg.Free;
end;   }

procedure TForm_opt.SpeedButton11Click(Sender: TObject);
var Reg: TRegistry;
begin
try
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  finally
    if Application.MessageBox(PChar(Form_main.ErrorMessage7),PChar(Form_main.Caption), MB_YESNO+MB_ICONQUESTION) = idYes then
      begin
      Reg.OpenKey('.xxc', True);
      Reg.WriteString('', 'xxcfile');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\DefaultIcon', True);
      Reg.WriteString('', extractfilepath(paramstr(0))+'FileIcon.ico');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\shell\open\command', True);
      Reg.WriteString('', Application.ExeName+' "%1"');
      Reg.CloseKey;
      Reg.OpenKey('xxcfile\', True);
      Reg.WriteString('', 'Executor 2.1 Task List');
      Reg.CloseKey;
      Label6.Caption:=IniLanguage.ReadString('options','624','');
      AssociationEx:=True;
      end;
  end;
Reg.Free;
end;

procedure TForm_opt.SpeedButton10Click(Sender: TObject);
var Reg: TRegistry;
begin
try
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  finally
    Reg.DeleteKey('.xxc');
    Reg.DeleteKey('xxcfile\');
    Label6.Caption:=IniLanguage.ReadString('options','625','');
    AssociationEx:=True;
  end;
Reg.Free;
end;

end.
