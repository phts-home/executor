unit uAbout;         //� ���������

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ShellApi, ImgList;

type
  TForm_help = class(TForm)
    Button_close: TButton;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Timer_show: TTimer;
    Timer_close: TTimer;
    Label10: TLabel;
    Label11: TLabel;
    procedure Label4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer_showTimer(Sender: TObject);
    procedure Timer_closeTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Label10Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_help: TForm_help;
  i:integer;

implementation

uses uMain;

{$R *.dfm}

procedure TForm_help.Label4Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Label4.Caption+'?subject=Executor 2.1'),'','',1);
end;

procedure TForm_help.FormShow(Sender: TObject);
begin
Form_help.Left:=Form_main.Left+40;
Form_help.Top:=Form_main.top+20;

Form_help.AlphaBlendValue:=0;
i:=0;
Timer_show.Enabled:=true;
end;

procedure TForm_help.Timer_showTimer(Sender: TObject);
begin
Form_help.AlphaBlendValue := i;
inc(i,15);
if i>255
  then
  begin
  Timer_show.Enabled:=false;
  end;
end;

procedure TForm_help.Timer_closeTimer(Sender: TObject);
begin
Form_help.AlphaBlendValue := i;
dec(i,15);
if i<=0
  then
  begin
  Timer_close.Enabled:=false;
  close;
  end;
end;

procedure TForm_help.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
CanClose:=false;
if i<=0
  then CanClose:=true
  else
  begin
  i:=255;
  Timer_close.Enabled:=true;
  end;
end;

procedure TForm_help.Label10Click(Sender: TObject);
begin
ShellExecute(0,'',pchar(Label10.Caption),'','',1);
end;

end.
