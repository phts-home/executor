unit uShutDown;           //����������

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin;

type
  TForm_exwin = class(TForm)
    Label_time02: TLabel;
    Label1: TLabel;
    Timer1: TTimer;
    Label_time: TLabel;
    Panel1: TPanel;
    Label2: TLabel;
    Shape1: TShape;
    procedure Label2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    Countdown:byte;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    { Public declarations }
  end;

var
  Form_exwin: TForm_exwin;

implementation

uses uMain, uOptions, uEdit, uAbout, uHelp, uFolder, uStopButton,
  uRadioButtons, uTimer;



{$R *.dfm}

procedure TForm_exwin.Label2Click(Sender: TObject);
begin
CLOSE;
end;

procedure TForm_exwin.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
end;

procedure TForm_exwin.Timer1Timer(Sender: TObject);
begin
Countdown:=Countdown-1;
if Countdown>9
  then label_time.Caption:=inttostr(Countdown)
  else label_time.Caption:='0'+inttostr(Countdown);
if Countdown=0
  then
    begin
    if Label1.Caption=Form_main.strShuting
      then winexec('shutdown -s -t 0',0)                //����
      else winexec('shutdown -r -t 0',0);               //��������
    Form_main.close;
    end;
end;

procedure TForm_exwin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
timer1.Enabled:=false;
label_time.Caption:=inttostr(form_opt.spinedit_timer.value);
end;

procedure TForm_exwin.FormShow(Sender: TObject);
begin
Countdown:=form_opt.SpinEdit_timer.value;
if Countdown>9
  then label_time.Caption:=inttostr(Countdown)
  else label_time.Caption:='0'+inttostr(Countdown);
Timer1.Enabled:=true;
Form_exwin.Left:=Form_main.Left+20;
Form_exwin.Top:=Form_main.top+10;
end;



end.
