program Executor;

uses
  Forms,
  uMain in 'uMain.pas' {Form_main},
  uEdit in 'uEdit.pas' {Form_edit},
  uAbout in 'uAbout.pas' {Form_help},
  uOptions in 'uOptions.pas' {Form_opt},
  uStopButton in 'uStopButton.pas' {Form_play},
//  uHelp in 'uHelp.pas' {Form_help2},
  uFolder in 'uFolder.pas' {Form_dir},
  uShutDown in 'uShutDown.pas' {Form_exwin},
  uRadioButtons in 'uRadioButtons.pas' {Form_max1},
  uTimer in 'uTimer.pas' {Form_max2},
  uSplash in 'uSplash.pas' {Form_splash};

//var i: Integer;
{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'EXECUTOR';
  Application.CreateForm(TForm_main, Form_main);
  Application.CreateForm(TForm_splash, Form_splash);
  Application.CreateForm(TForm_max1, Form_max1);
  Application.CreateForm(TForm_max2, Form_max2);
  Application.CreateForm(TForm_help, Form_help);
  Application.CreateForm(TForm_play, Form_play);
//  Application.CreateForm(TForm_help2, Form_help2);
  Application.CreateForm(TForm_dir, Form_dir);
  Application.CreateForm(TForm_edit, Form_edit);
  //  Application.CreateForm(TForm_exwin, Form_exwin);
  Application.CreateForm(TForm_opt, Form_opt);
  Form_main.ReadIniFile(Form_main.CloserFile);
  Application.CreateForm(TForm_exwin, Form_exwin);
//  Form_main.ReadIniFile(Form_main.CloserFile);
  Form_main.Ini.WriteString('options','language',Form_opt.CurrentLanguage);
  Form_main.SetFocus;
  if Form_opt.CheckBox1.Checked
    then
    begin
    Form_main.ReadXXCFile(Form_opt.Edit1.Text);
    end;
  if Form_main.N18.Checked
    then
    begin
    Form_splash.Close;
    Form_main.N20.OnClick(Form_main.N20);
    end;
////
  Form_main.OpeningFile:='';
  if ParamCount>0 then
    begin
    Form_main.OpeningFile:=ParamStr(1);
{    for i:=2 to ParamCount do
      Form_main.OpeningFile:=Form_main.OpeningFile+ParamStr(i); }
    end;
  if Form_main.OpeningFile<>'' then
    begin
    Form_main.ReadXXCFile(Form_main.OpeningFile);
    Form_opt.ChangeLanguage(Form_opt.CurrentLanguage);
    Form_main.OpenDialog_open.FileName:='';
    if form_max2.ListBox_prog.Items.Count<>0 then form_max2.Timer1.Enabled:=true;
    end;
////
  Application.run;
end.
