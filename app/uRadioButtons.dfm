object Form_max1: TForm_max1
  Left = 836
  Top = 157
  BorderStyle = bsNone
  Caption = 'Form_max1'
  ClientHeight = 157
  ClientWidth = 115
  Color = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWhite
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 60
    Top = 128
    Width = 48
    Height = 24
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B000000000000000000000000FF0000FF
      0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00000000
      00000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF000000007AB90000000000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00000000
      00000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00000000
      00000000000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF00000025AAFF007AB925AAFF0000000000000000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00000000000000000000
      0000007AB925AAFF007AB90000000000FF0000FF0000FF0000FF0000FF0000FF
      0000FF00000000000025AAFF007AB9000000000000007AB925AAFF0000000000
      FF0000FF0000FF0000FF0000FF0000FF0000FF000000007AB925AAFF25AAFF25
      AAFF25AAFF25AAFF007AB90000000000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF000000006296007AB925AAFF25AAFF0062960000000000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00000000000000
      00000000000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF}
    OnClick = SpeedButton1Click
    OnMouseMove = SpeedButton1MouseMove
    OnMouseUp = SpeedButton1MouseUp
  end
  object Panel1: TPanel
    Left = 5
    Top = 0
    Width = 106
    Height = 128
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    OnMouseUp = FormMouseUp
    object RadioButton_file: TRadioButton
      Left = 6
      Top = 4
      Width = 90
      Height = 15
      Caption = #1060#1072#1081#1083
      TabOrder = 0
      OnClick = RadioButton_fileClick
      OnMouseUp = RadioButton_fileMouseUp
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 20
      Width = 102
      Height = 51
      Caption = ' '#1055#1088#1086#1075#1088#1072#1084#1084#1072' '
      TabOrder = 1
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 72
      Width = 102
      Height = 51
      Caption = ' '#1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086': '
      TabOrder = 2
    end
    object RadioButton_go: TRadioButton
      Left = 6
      Top = 34
      Width = 90
      Height = 15
      Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = RadioButton_killClick
      OnMouseUp = RadioButton_killMouseUp
    end
    object RadioButton_kill: TRadioButton
      Left = 6
      Top = 49
      Width = 90
      Height = 15
      Caption = #1059#1073#1080#1090#1100
      TabOrder = 4
      OnClick = RadioButton_killClick
      OnMouseUp = RadioButton_killMouseUp
    end
    object RadioButton_capt: TRadioButton
      Left = 6
      Top = 86
      Width = 90
      Height = 15
      Caption = #1087#1086' '#1079#1072#1075#1086#1083#1086#1074#1082#1091
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 5
      OnClick = RadioButton_captClick
      OnMouseUp = RadioButton_captMouseUp
    end
    object RadioButton_class: TRadioButton
      Left = 6
      Top = 101
      Width = 90
      Height = 15
      Caption = #1087#1086' '#1082#1083#1072#1089#1089#1091
      TabOrder = 6
      OnClick = RadioButton_captClick
      OnMouseUp = RadioButton_captMouseUp
    end
  end
  object SpinEdit_val: TSpinEdit
    Left = 7
    Top = 128
    Width = 48
    Height = 22
    EditorEnabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxValue = 9
    MinValue = 1
    ParentFont = False
    TabOrder = 1
    Value = 1
    OnChange = SpinEdit_valChange
    OnMouseDown = SpinEdit_valMouseDown
    OnMouseUp = FormMouseUp
  end
end
