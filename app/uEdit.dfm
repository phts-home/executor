object Form_edit: TForm_edit
  Left = 437
  Top = 80
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1088#1072#1074#1082#1072
  ClientHeight = 98
  ClientWidth = 354
  Color = 13160660
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000040040000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000FF000000FF0000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000FFC2C2C2FF0000
    00FF000000FF0000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000FFC2C2C2FFFFFFFFFFFFFF
    FFFF9B9B9BFF000000FF000000FF000000000000000000000000000000000000
    000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
    FFFF4F4F4FFF020202FFBDBDBDFF000000FF000000FF00000000000000000000
    0000000000000000000000000000000000FFC2C2C2FFFFFFFFFFFFFFFFFFFFFF
    FFFF121212FF1F1F1FFFFFFFFFFFFFFFFFFFC2C2C2FF000000FF000000000000
    0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFD2D2D2FF9E9E
    9EFF010101FF777777FFFFFFFFFFFFFFFFFFFFFFFFFFDADADAFF000000FF0000
    00000000000000000000000000FFC2C2C2FFD8D8D8FF292D2FFF000000FF0000
    00FF000000FF404142FFF0F0F0FFFFFFFFFFDADADAFF000000FF000000000000
    00000000000000000000000000FFF3F3F3FF14181AFF030C12FF10476BFF1769
    9DFF13547FFF050B0FFF525354FFFFFFFFFFDADADAFF000000FF000000000000
    000000000000000000FFC2C2C2FF858686FF010304FF104C72FF1A77B3FF2199
    E6FF39B2FFFF3A7FABFF000000FFC6C6C6FF000000FF00000000000000000000
    000000000000000000FFFFFFFFFF555656FF05151FFF186DA4FF1F90D8FF32AF
    FFFF59BEFFFF6FC1F6FF000000FF838383FF000000FF00000000000000000000
    0000000000FFC2C2C2FFFFFFFFFF666666FF0D1418FF1D83C5FF24A5F8FF4AB8
    FFFF6EC6FFFF79BAE4FF000000FF000000FF0000000000000000000000000000
    0000000000FFFFFFFFFFFFFFFFFFC3C3C3FF000000FF45687FFF3EB4FFFF53BC
    FFFF8BCCF6FF484E51FF0F0F0FFF000000FF0000000000000000000000000000
    00FFC2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFF7E7E7EFF000000FF2D2D2DFF6060
    60FF1B1B1BFF090909FF000000FF000000000000000000000000000000000000
    0000000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
    00FF000000FF000000FF000000FF000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    0000FCFF0000FC3F0000F80F0000F8030000F0010000F0000000E0010000E001
    0000C0030000C00300008007000080070000000F0000800F0000FFFF0000}
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 145
    Top = 7
    Width = 23
    Height = 22
    Caption = #1041
    Flat = True
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 169
    Top = 7
    Width = 23
    Height = 22
    Caption = #1057
    Flat = True
    OnClick = SpeedButton2Click
  end
  object SpeedButton3: TSpeedButton
    Left = 193
    Top = 7
    Width = 23
    Height = 22
    Caption = #1055
    Flat = True
    OnClick = SpeedButton3Click
  end
  object SpeedButton4: TSpeedButton
    Left = 217
    Top = 7
    Width = 23
    Height = 22
    Caption = #1042
    Flat = True
    OnClick = SpeedButton4Click
  end
  object Edit_programs: TEdit
    Left = 5
    Top = 36
    Width = 191
    Height = 21
    CharCase = ecLowerCase
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = Edit_programsChange
  end
  object DateTimePicker_date: TDateTimePicker
    Left = 5
    Top = 8
    Width = 79
    Height = 21
    Date = 38532.553857060190000000
    Time = 38532.553857060190000000
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object MaskEdit_time: TMaskEdit
    Left = 88
    Top = 8
    Width = 54
    Height = 21
    EditMask = '!90:00:00;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 8
    ParentFont = False
    TabOrder = 2
    Text = '15:32:46'
    OnChange = MaskEdit_timeChange
    OnKeyDown = MaskEdit_timeKeyDown
    OnMouseUp = MaskEdit_timeMouseUp
  end
  object Button_save: TButton
    Left = 5
    Top = 66
    Width = 72
    Height = 25
    Caption = #1047#1072#1087#1086#1084#1085#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = Button_saveClick
  end
  object Button_cancel: TButton
    Left = 85
    Top = 66
    Width = 72
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 4
  end
  object Button_add: TButton
    Left = 165
    Top = 66
    Width = 72
    Height = 25
    Caption = #1054#1073#1079#1086#1088
    TabOrder = 5
    OnClick = Button_addClick
  end
  object edit_time_temp: TEdit
    Left = 25
    Top = 37
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'edit_time_temp'
    Visible = False
  end
  object SpinEdit_val: TSpinEdit
    Left = 203
    Top = 36
    Width = 33
    Height = 22
    EditorEnabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxValue = 9
    MinValue = 1
    ParentFont = False
    TabOrder = 7
    Value = 1
    OnMouseDown = SpinEdit_valMouseDown
  end
  object Panel3: TPanel
    Left = 245
    Top = 2
    Width = 106
    Height = 99
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 8
    object RadioButton_kill: TRadioButton
      Left = 7
      Top = 31
      Width = 89
      Height = 15
      Caption = #1059#1073#1080#1090#1100
      TabOrder = 0
      OnClick = RadioButton_killClick
      OnMouseUp = RadioButton_killMouseUp
    end
    object RadioButton_go: TRadioButton
      Left = 7
      Top = 17
      Width = 89
      Height = 15
      Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = RadioButton_killClick
      OnMouseUp = RadioButton_killMouseUp
    end
    object RadioGroup1: TRadioGroup
      Left = 1
      Top = 45
      Width = 102
      Height = 45
      Caption = ' '#1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086': '
      TabOrder = 2
    end
    object RadioButton_capt: TRadioButton
      Left = 8
      Top = 58
      Width = 89
      Height = 15
      Caption = #1087#1086' '#1079#1072#1075#1086#1083#1086#1074#1082#1091
      TabOrder = 3
      OnClick = RadioButton_captClick
      OnMouseUp = RadioButton_captMouseUp
    end
    object RadioButton_class: TRadioButton
      Left = 8
      Top = 72
      Width = 89
      Height = 15
      Caption = #1087#1086' '#1082#1083#1072#1089#1089#1091
      TabOrder = 4
      OnClick = RadioButton_captClick
      OnMouseUp = RadioButton_captMouseUp
    end
    object RadioButton_file: TRadioButton
      Left = 7
      Top = 3
      Width = 89
      Height = 15
      Caption = #1060#1072#1081#1083
      TabOrder = 5
      OnClick = RadioButton_fileClick
      OnMouseUp = RadioButton_killMouseUp
    end
  end
  object OpenDialog_addexe: TOpenDialog
    Filter = 'Programs (*.exe)|*.exe'
    Left = 26
    Top = 32
  end
  object OpenDialog_addfile: TOpenDialog
    Filter = 'Files (*.*)|*.*'
    Left = 61
    Top = 30
  end
end
