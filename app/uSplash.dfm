object Form_splash: TForm_splash
  Left = 291
  Top = 170
  Cursor = crHourGlass
  BorderStyle = bsNone
  Caption = 'Form_splash'
  ClientHeight = 161
  ClientWidth = 376
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 0
    Top = 0
    Width = 376
    Height = 161
    Shape = bsFrame
  end
  object Bevel1: TBevel
    Left = 5
    Top = 5
    Width = 365
    Height = 150
    Shape = bsFrame
    Style = bsRaised
  end
  object Label2: TLabel
    Left = 40
    Top = 15
    Width = 292
    Height = 62
    Alignment = taCenter
    Caption = 'EXECUTOR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -53
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 15
    Top = 95
    Width = 107
    Height = 14
    Caption = #1063#1058#1045#1053#1048#1045' '#1053#1040#1057#1058#1056#1054#1045#1050'...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object ProgressBar1: TProgressBar
    Left = 15
    Top = 110
    Width = 346
    Height = 32
    Smooth = True
    TabOrder = 0
  end
end
