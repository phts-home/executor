unit uTimer;           //timer

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, Spin, Buttons, Menus, shellAPI,
  ImgList;

type
  TForm_max2 = class(TForm)
    MaskEdit: TMaskEdit;
    ListBox_prog: TListBox;
    Timer1: TTimer;
    Panel1: TPanel;
    RadioButton_kill: TRadioButton;
    RadioButton_go: TRadioButton;
    RadioGroup1: TRadioGroup;
    RadioButton_capt: TRadioButton;
    RadioButton_class: TRadioButton;
    ComboBox_prog: TComboBox;
    OpenDialog_addexe: TOpenDialog;
    edit_time_temp: TEdit;
    GroupBox1: TGroupBox;
    RadioButton_file: TRadioButton;
    OpenDialog_addfile: TOpenDialog;
    ImgLst_clock: TImageList;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    ImageList2: TImageList;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MaskEditChange(Sender: TObject);
    procedure MaskEditMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox_progChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RadioButton_killClick(Sender: TObject);
    procedure RadioButton_captClick(Sender: TObject);
    procedure MaskEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Panel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure RadioButton_killMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_captMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_fileClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
  prog1,capt1,file11:boolean;
  tray_button7,                     //��������� � �������
  tray_button8,                     //������� ���������� �����
  tray_button9,                     //���������� / ��������� ������
  tray_button10,                    //�������� ����� ��������� / ����
  ButPause,                         //�����
  ButCon:string;                    //�����
  num:byte;
  b,bb:TBitMap;
    { Public declarations }
  end;

var
  Form_max2: TForm_max2;


implementation

uses uMain, uRadioButtons, uShutDown, uOptions, uStopButton;



{$R *.dfm}

procedure TForm_max2.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.SetFocus;
if (form_max1.radiobutton_capt.Checked)or(form_max1.radiobutton_class.Checked)
  then
  begin
  if Form_main.combobox_prog.Text='""'
    then
    begin
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
  end
  else
  if (form_max1.radiobutton_kill.Checked)or(form_max1.radiobutton_go.Checked)
    then Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);

end;

procedure TForm_max2.MaskEditChange(Sender: TObject);
var t:integer;
begin
t:=maskedit.SelStart-1;
if MaskEdit.text[4]>'5'
  then
  begin
  MaskEdit.text:=copy(edit_time_temp.text,1,3)+'5'+copy(edit_time_temp.text,5,5);
  maskedit.SelStart:=t+1;
  end;
if MaskEdit.text[7]>'5'
  then
  begin
  MaskEdit.text:=copy(edit_time_temp.text,1,7)+'5'+copy(edit_time_temp.text,8,1);
  maskedit.SelStart:=t+1;
  end;
edit_time_temp.Text:=MaskEdit.text;
end;

procedure TForm_max2.MaskEditMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
case x of
  1..7: maskedit.SelStart:=0;
  8..14: maskedit.SelStart:=1;
  15..16: maskedit.SelStart:=2;
  17..23: maskedit.SelStart:=3;
  24..30: maskedit.SelStart:=4;
  31..32: maskedit.SelStart:=5;
  33..39: maskedit.SelStart:=6;
  40..56: maskedit.SelStart:=7;
end;
end;

procedure TForm_max2.FormCreate(Sender: TObject);
begin
form_max2.Left:=Form_main.Left;
form_max2.Top:=Form_main.Top+Form_main.Height;
edit_time_temp.Text:=MaskEdit.text;
prog1:=true;
Form_main.SetFocus;
end;

procedure TForm_max2.ComboBox_progChange(Sender: TObject);
var s:string;
begin
if (pos('.exe',ComboBox_prog.Text)>4)and(pos(':\',ComboBox_prog.Text)=2)
  then
    begin
    if (not radiobutton_go.Checked)and(not radiobutton_kill.Checked)
      then
        begin
        s:=ComboBox_prog.Text;
        radiobutton_go.Checked:=true;
        ComboBox_prog.Text:=s;
        ComboBox_prog.Selstart:=length(ComboBox_prog.text);
        end;
    end;

if (pos('"',ComboBox_prog.Text)<>0)or(pos('>',ComboBox_prog.Text)<>0)or(pos('|',ComboBox_prog.Text)<>0)or(pos('/',ComboBox_prog.Text)<>0)
        or(pos('*',ComboBox_prog.Text)<>0)or(pos('?',ComboBox_prog.Text)<>0)
  then
    begin
    if (not radiobutton_capt.Checked)and(not radiobutton_class.Checked)
      then
        begin
        s:=ComboBox_prog.Text;
        radiobutton_capt.Checked:=true;
        ComboBox_prog.Text:=s;
        ComboBox_prog.Selstart:=length(ComboBox_prog.text);
        end;
    end;
end;

procedure Check (text:string ; var state:boolean);
var s:string;
    i,m:integer;
begin
state:=true;
m:=0;

if (text<>Form_main.strMessage)and(text<>Form_main.strReboot)and(text<>Form_main.strShut)
  then
    for i:=1 to length(text) do
      begin
      if text[i]='"'
        then m:=m+1;
      end;

if text<>''
then

if ((pos('.exe',text)>4)and(text[length(text)]='e')and(text[length(text)-1]='x')and(text[length(text)-2]='e')
                          and(text[length(text)-3]='.')and(pos(':\',text)=2)
                          and((form_max2.radiobutton_kill.checked)or(form_max2.radiobutton_go.checked)))or
                          (text=Form_main.strMessage)or(text=Form_main.strShut)or(text=Form_main.strReboot)
                          or((pos('"',text)=1)and (m>=2)and(text[length(text)]='"')
                          or((form_max2.radiobutton_file.checked)and(text[length(text)-3]='.')and(pos(':\',text)=2)))
  then
    begin
    if (pos('<',text)=0) and (pos(':\',text)<>0) and(pos('.',text)<>0)
                            and (not form_max2.radiobutton_capt.Checked) and (not form_max2.radiobutton_class.Checked)
      then
        begin
        s:=text;
        for i:=0 to (form_max2.combobox_prog.items.count-1) do
          begin
          if s=form_max2.combobox_prog.items[i]
            then form_max2.combobox_prog.items.Delete(i);
          end;
        form_max2.combobox_prog.items.Insert(0,s);
        form_max2.combobox_prog.text:=s;
        if form_max2.combobox_prog.items.count=11
          then form_max2.combobox_prog.items.Delete(10);
        end;

    if (form_max2.radiobutton_capt.Checked)and(text<>Form_main.strMessage)and(text<>Form_main.strReboot)and(text<>Form_main.strShut)
      then
        begin
        s:=text;
        for i:=0 to (form_max2.combobox_prog.items.count-1) do
          begin
          if s=form_max2.combobox_prog.items[i]
            then form_max2.combobox_prog.items.Delete(i);
          end;
        form_max2.combobox_prog.items.Insert(0,s);
        form_max2.combobox_prog.text:=s;
        if form_max2.combobox_prog.items.count=11
          then form_max2.combobox_prog.items.Delete(10);
        end;

    if (form_max2.radiobutton_class.Checked)and(text<>Form_main.strMessage)and(text<>Form_main.strReboot)and(text<>Form_main.strShut)
      then
        begin
        s:=text;
        for i:=0 to (form_max2.combobox_prog.items.count-1) do
          begin
          if s=form_max2.combobox_prog.items[i]
            then form_max2.combobox_prog.items.Delete(i);
          end;
        form_max2.combobox_prog.items.Insert(0,s);
        form_max2.combobox_prog.text:=s;
        if form_max2.combobox_prog.items.count=11
          then form_max2.combobox_prog.items.Delete(10);
        end;
    end
  else
    begin
    Application.MessageBox(pchar(Form_main.ErrorMessage2),pchar(Form_main.Caption),mb_IconError+mb_Ok);
    state:=false;
    form_max2.combobox_prog.SetFocus;
    end
else
  begin
  state:=false;
  form_max2.combobox_prog.Text:='';
  form_max2.combobox_prog.SetFocus;
  end;
end;

procedure TForm_max2.Timer1Timer(Sender: TObject);
var i,h,sec,min,hour:integer;
    st,ttt,ssec,smin,shour:string;
    soob:boolean;
    buffer: array [0..255] of char;
begin
inc(num);
if num=5 then num:=0;
b:=TBitMap.Create;
ImgLst_clock.GetBitmap(num,b);
Form_main.BitBtn4.Glyph:=b;

soob:=false;
for i:=0 to ListBox_prog.Items.Count-1 do
  begin
  st:=ListBox_prog.Items[i];
  sec:=strtoint(copy(st,7,2));
  min:=strtoint(copy(st,4,2));
  hour:=strtoint(copy(st,1,2));
  ttt:=copy(st,9,length(st)-8);
  if (sec=0)and(min=0)and(hour=0)
    then
      begin
      if (pos('<',listbox_prog.items[i])=0) and (copy(listbox_prog.items[i],24,3)<>'(=)') and
                      (copy(listbox_prog.items[i],24,3)<>'(#)')
        then
          begin

          if copy(listbox_prog.items[i],12,3)='(+)'
            then
              begin
              winexec(pchar(copy(listbox_prog.items[i],18,(length(listbox_prog.items[i])-17))),1);
              end;
          if copy(listbox_prog.items[i],12,3)='(x)'
            then
              begin
              winexec(pchar('taskkill /f /im '+ExtractFileName(copy(listbox_prog.items[i],29,(length(listbox_prog.items[i]))))),0);
              end;
          end;
      if copy(listbox_prog.items[i],12,3)='(F)'
            then
              begin
              GetCurrentDirectory(sizeof(buffer),buffer);
              shellexecute(0,nil,pchar(copy(listbox_prog.items[i],18,(length(listbox_prog.items[i])-17))),nil,buffer,SW_SHOW);
              end;
      if copy(listbox_prog.items[i],12,3)='(=)'
        then
          begin
            h:=findwindow(nil,pchar(copy(listbox_prog.items[i],19,(length(listbox_prog.items[i])-19))));
            sendmessage(h,wm_close,0,0);
          end;

      if copy(listbox_prog.items[i],12,3)='(#)'
        then
          begin
            h:=findwindow(pchar(copy(listbox_prog.items[i],19,(length(listbox_prog.items[i])-19))),nil);
            sendmessage(h,wm_close,0,0);
          end;

      if pos(Form_main.strShut,listbox_prog.items[i])=18
        then
          begin
          form_exwin.Close;
          if Form_opt.CheckBox_timer.Checked=true
            then form_exwin.show;
          form_exwin.Label1.Caption:=Form_main.strShuting;
          end;

      if pos(Form_main.strReboot,listbox_prog.items[i])=18
        then
          begin
          form_exwin.Close;
          form_exwin.show;
          form_exwin.Label1.Caption:=Form_main.strRebooting;
          end;

      if pos(Form_main.strMessage,listbox_prog.items[i])=18
        then
          begin
          soob:=true;
          if form_opt.checkbox_mus.checked=true
            then
              try begin
              with Form_main.MediaPlayer1 do
                begin
                Close;
                FileName:=form_opt.edit_soob_mus.text;
                Open;
                Play;
                end;
              form_play.show;
              ListBox_prog.Items.Delete(i);
              application.MessageBox(pchar(form_opt.edit_soob.text),pchar(Form_main.caption),MB_IconWarning+MB_OK);
              end;
              except
                begin
                ListBox_prog.Items.Delete(i);
                Application.MessageBox(pchar(Form_main.ErrorMessage1+#13+form_opt.edit_soob_mus.text),pchar(Form_main.Caption),mb_IconError+mb_Ok);
//                messagedlg(Form_main.ErrorMessage1,mterror,[mbok],0);
//                form_opt.showmodal;
                end;
              end
            else
              application.MessageBox(pchar(form_opt.edit_soob.text),pchar(Form_main.caption),MB_IconWarning+MB_OK);
          end;


      if not soob then ListBox_prog.Items.Delete(i);
      if ListBox_prog.items.Count=0 then
        begin
        timer1.Enabled:=false;

        b:=TBitMap.Create;
        num:=0;
        ImgLst_clock.GetBitmap(num,b);
        Form_main.BitBtn4.Glyph:=b;
        end;
      Form_main.CheckHorizontalBar(form_max2.listbox_prog);;
      break;
      end;
  sec:=sec-1;
  if sec=-1
    then
      begin
      sec:=59;
      min:=min-1;
      if min=-1
        then
          begin
          min:=59;
          hour:=hour-1;
          if hour=-1
            then
              begin
              hour:=0;
              end;
          end;
      end;
  if length(inttostr(sec))<>1
    then ssec:=inttostr(sec)
    else ssec:='0'+inttostr(sec);
  if length(inttostr(min))<>1
    then smin:=inttostr(min)
    else smin:='0'+inttostr(min);
  if length(inttostr(hour))<>1
    then shour:=inttostr(hour)
    else shour:='0'+inttostr(hour);
  ListBox_prog.Items[i]:=concat(shour,':',smin,':',ssec,ttt);
  end;
end;

procedure TForm_max2.RadioButton_killClick(Sender: TObject);
var s,st:string;
    t:textFile;
begin
if not prog1 then
  begin
  if capt1
    then form_max1.WriteItems(combobox_prog.Items,'[timer captions]');
  if file11
    then form_max1.WriteItems(combobox_prog.Items,'[timer files]');
  s:='';
  combobox_prog.CharCase:=ecLowerCase;
  combobox_prog.Clear;

  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[timer programs]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);
  end
  else s:=combobox_prog.Text;
prog1:=true;
capt1:=false;
file11:=false;
combobox_prog.Text:=s;
combobox_prog.Selstart:=length(combobox_prog.Text);
end;

procedure TForm_max2.RadioButton_captClick(Sender: TObject);
var s,st:string;
    t:textFile;
begin
if not capt1 then
  begin
  if prog1
    then form_max1.WriteItems(combobox_prog.Items,'[timer programs]');
  if file11
    then form_max1.WriteItems(combobox_prog.Items,'[timer files]');
  if combobox_prog.Text=''
    then
    begin
    combobox_prog.Text:='';
    combobox_prog.Selstart:=2;
    end;
  combobox_prog.CharCase:=ecnormal;
  combobox_prog.Clear;

  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[timer captions]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);
  end
  else s:=combobox_prog.Text;
prog1:=false;
capt1:=true;
file11:=false;
combobox_prog.Text:=s;
if (combobox_prog.Text='')or(combobox_prog.Text='""')or(pos('<',combobox_prog.Text)<>0)
    then
    begin
    combobox_prog.Text:='""';
    combobox_prog.Selstart:=1;
    end
    else combobox_prog.Selstart:=length(combobox_prog.Text)-1;

end;

procedure TForm_max2.RadioButton_fileClick(Sender: TObject);
var s,st:string;
    t:textFile;
begin
if not file11
  then
  begin
  if prog1
    then form_max1.WriteItems(combobox_prog.Items,'[timer programs]');
  if capt1
    then form_max1.WriteItems(combobox_prog.Items,'[timer captions]');
  s:='';
  combobox_prog.CharCase:=ecLowerCase;
  combobox_prog.Clear;
  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[timer files]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);
  end
  else s:=combobox_prog.Text;
prog1:=false;
capt1:=false;
file11:=true;
combobox_prog.Text:=s;
combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);
end;

procedure TForm_max2.MaskEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=46 then key:=0;
if key=8 then key:=37;
end;

procedure TForm_max2.Panel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.SetFocus;
end;

procedure TForm_max2.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Form_main.statusbar1.panels[0].text:='';
end;

procedure TForm_max2.RadioButton_killMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
combobox_prog.SetFocus;
combobox_prog.Selstart:=length(combobox_prog.Text);
if pos('<',combobox_prog.Text)<>0
  then combobox_prog.Text:='';
end;

procedure TForm_max2.RadioButton_captMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
combobox_prog.SetFocus;
if (combobox_prog.Text='')or(combobox_prog.Text='""')or (pos('<',combobox_prog.Text)=1)
    then
    begin
    combobox_prog.Text:='""';
    combobox_prog.Selstart:=1;
    end
    else combobox_prog.Selstart:=length(combobox_prog.Text)-1;
end;

procedure TForm_max2.BitBtn1Click(Sender: TObject);
var sr,tt:string;
    bbb:boolean;
begin
tt:=ComboBox_prog.Text;
check(tt,bbb);
if bbb
  then begin
timer1.Enabled:=true;

bb:=TBitMap.Create;
ImageList2.GetBitmap(0,bb);
BitBtn3.Glyph:=bb;
BitBtn3.Caption:=ButPause;

if (tt<>Form_main.strMessage)and(tt<>Form_main.strReboot)and(tt<>Form_main.strShut)
  then begin
if radiobutton_file.Checked then sr:='(F)';
if radiobutton_kill.Checked then sr:='(x)';
if radiobutton_go.Checked then sr:='(+)';
if radiobutton_capt.Checked then sr:='(=)';
if radiobutton_class.Checked then sr:='(#)';
    end
  else sr:='   ';
ListBox_prog.Items.Add(maskedit.Text+'   '+sr+'   '+tt);
  if (tt=Form_main.strMessage)or(tt=Form_main.strReboot)or(tt=Form_main.strShut)
    then ComboBox_prog.Text:='';
    end;
Form_main.CheckHorizontalBar(form_max2.listbox_prog);
combobox_prog.SetFocus;

end;

procedure TForm_max2.BitBtn2Click(Sender: TObject);
begin
ListBox_prog.items.delete(ListBox_prog.itemindex);
if ListBox_prog.items.Count=0 then
  begin
  timer1.Enabled:=false;

  b:=TBitMap.Create;
  num:=0;
  ImgLst_clock.GetBitmap(num,b);
  Form_main.BitBtn4.Glyph:=b;

  bb:=TBitMap.Create;
  ImageList2.GetBitmap(0,bb);
  BitBtn3.Glyph:=bb;
  BitBtn3.Caption:=ButPause;

  BitBtn3.Caption:=ButPause;
  end;
Form_main.CheckHorizontalBar(form_max2.listbox_prog);
ListBox_prog.SetFocus;
end;

procedure TForm_max2.BitBtn3Click(Sender: TObject);
begin
b:=TBitMap.Create;
bb:=TBitMap.Create;
num:=0;
ImgLst_clock.GetBitmap(num,b);
Form_main.BitBtn4.Glyph:=b;
if timer1.Enabled then
  begin
  if listbox_prog.Items.Count<>0 then
    begin
    timer1.Enabled:=false;
    ImageList2.GetBitmap(1,bb);
    BitBtn3.Glyph:=bb;
    BitBtn3.Caption:=ButCon;
    end
    else
    begin
    timer1.Enabled:=true;
    ImageList2.GetBitmap(0,bb);
    BitBtn3.Glyph:=bb;
    BitBtn3.Caption:=ButPause;
    end;
  end
  else
  begin
  if listbox_prog.Items.Count<>0 then
    begin
    timer1.Enabled:=true;
    ImageList2.GetBitmap(0,bb);
    BitBtn3.Glyph:=bb;
    BitBtn3.Caption:=ButPause;
    end;
  end;

end;

procedure TForm_max2.BitBtn4Click(Sender: TObject);
begin
if (prog1 or capt1)and(opendialog_addexe.execute)
  then
    begin
    if not RadioButton_kill.checked
      then RadioButton_go.Checked:=true;
    prog1:=true;
    capt1:=false;
    file11:=false;
    ComboBox_prog.text:=opendialog_addexe.filename;
    end;
if file11 and(opendialog_addfile.execute)
  then
    begin
    ComboBox_prog.text:=opendialog_addfile.filename;
    end;
try ComboBox_prog.SetFocus;
  except end;
end;

procedure TForm_max2.BitBtn2MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button8;
end;

procedure TForm_max2.BitBtn4MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button10;
end;

procedure TForm_max2.BitBtn3MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button9;
end;

procedure TForm_max2.BitBtn1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button7;
end;

procedure TForm_max2.BitBtn1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.SetFocus;
end;

end.
