unit uEdit;           //������

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, ComCtrls, Spin, Buttons;

type
  TForm_edit = class(TForm)
    Edit_programs: TEdit;
    DateTimePicker_date: TDateTimePicker;
    MaskEdit_time: TMaskEdit;
    Button_save: TButton;
    Button_cancel: TButton;
    Button_add: TButton;
    OpenDialog_addexe: TOpenDialog;
    edit_time_temp: TEdit;
    SpinEdit_val: TSpinEdit;
    Panel3: TPanel;
    RadioButton_kill: TRadioButton;
    RadioButton_go: TRadioButton;
    RadioGroup1: TRadioGroup;
    RadioButton_capt: TRadioButton;
    RadioButton_class: TRadioButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    RadioButton_file: TRadioButton;
    OpenDialog_addfile: TOpenDialog;
    procedure Button_addClick(Sender: TObject);
    procedure Button_saveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit_programsChange(Sender: TObject);
    procedure RadioButton_killClick(Sender: TObject);
    procedure RadioButton_captClick(Sender: TObject);
    procedure MaskEdit_timeChange(Sender: TObject);
    procedure SpinEdit_valMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MaskEdit_timeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MaskEdit_timeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure RadioButton_captMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_killMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_fileClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
  prog2,capt2,file2:boolean;
  procedure Min;
  procedure Sec;
  procedure Hour;
    { Public declarations }
  end;

var
  Form_edit: TForm_edit;

implementation

uses  uMain,uRadioButtons;

{$R *.dfm}


procedure TForm_edit.Button_addClick(Sender: TObject);
begin
if (prog2 or capt2) and(opendialog_addexe.execute)
  then
    begin
    if not form_max1.RadioButton_kill.checked
      then form_max1.RadioButton_go.Checked:=true;
    form_max1.prog:=true;
    form_max1.capt:=false;
    form_max1.file1:=false;
    Edit_programs.text:=opendialog_addexe.filename;
    end;
if file2 and(opendialog_addfile.execute)
  then
    begin
    Edit_programs.text:=opendialog_addfile.filename;
    end;
Edit_programs.SetFocus;
end;

procedure TForm_edit.Button_saveClick(Sender: TObject);
var s:string;
    b1,b2:boolean;
begin
if (edit_programs.Text=Form_main.strAlarm)or(edit_programs.Text=Form_main.strMessage)
      or(edit_programs.Text=Form_main.strShut)or(edit_programs.Text=Form_main.strreboot)
  then
  begin
  s:='   ';
  end
  else
  begin
  if radiobutton_go.Checked
    then s:='(+)';
  if radiobutton_kill.Checked
    then s:='(x)';
  if radiobutton_capt.Checked
    then s:='(=)';
  if radiobutton_class.Checked
    then s:='(#)';
  if radiobutton_file.Checked
    then s:='(F)';
  end;

b1:=(((s='(#)')or(s='(=)'))and(pos('"',edit_programs.Text)=1)and(Edit_programs.Text[length(Edit_programs.Text)]='"'))
      or(((s='(+)')or(s='(x)'))and(pos('.exe',edit_programs.Text)>4)
      and(pos(':\',edit_programs.Text)=2)and(copy(edit_programs.Text,length(edit_programs.Text)-3,4)='.exe')
      or((s='(F)')and(pos(':\',edit_programs.Text)=2)));

b2:=(s='   ')and((edit_programs.Text=Form_main.strAlarm)or(edit_programs.Text=Form_main.strMessage)
      or(edit_programs.Text=Form_main.strShut)or(edit_programs.Text=Form_main.strreboot));


if (b1)or(b2)
  then
  begin
    if maskedit_time.Text[1]='0'
      then
        begin
        if (b2)
          then
          begin
          Form_main.listbox_programs.items.Add(datetostr(datetimepicker_date.date)+'    '+copy(maskedit_time.text,2,7)+'  '+s+'  '+edit_programs.text)
          end
          else
          begin
          Form_main.listbox_programs.items.Add(datetostr(datetimepicker_date.date)+'    '+copy(maskedit_time.text,2,7)+'  '+s+'  '+edit_programs.text+'  (*'+inttostr(SpinEdit_val.Value)+')')
          end;
        end
      else
        begin
        if (b2)
          then
          begin
          Form_main.listbox_programs.items.Add(datetostr(datetimepicker_date.date)+'   '+maskedit_time.text+'  '+s+'  '+edit_programs.text)
          end
          else
          begin
          Form_main.listbox_programs.items.Add(datetostr(datetimepicker_date.date)+'   '+maskedit_time.text+'  '+s+'  '+edit_programs.text+'  (*'+inttostr(SpinEdit_val.Value)+')');
          end;
        end;
    modalresult:=mrok;
    end


  else
    begin
    messagedlg(Form_main.ErrorMessage2,mterror,[mbok],0);
    modalresult:=mrnone;
    edit_programs.SetFocus;
    end;

end;

procedure TForm_edit.FormShow(Sender: TObject);
begin
form_edit.Left:=Form_main.Left+40;
form_edit.Top:=Form_main.Top+10;
edit_programs.SetFocus;
end;

procedure TForm_edit.Edit_programsChange(Sender: TObject);
var s:string;
begin
if (pos('.exe',edit_programs.Text)>4)and(pos(':\',edit_programs.Text)=2)
  then
    begin
    if (not radiobutton_go.Checked)and(not radiobutton_kill.Checked)
      then
        begin
        s:=edit_programs.Text;
        radiobutton_go.Checked:=true;
        edit_programs.Text:=s;
        edit_programs.Selstart:=length(edit_programs.text);
        end;
    end;


if ((pos('"',edit_programs.Text)<>0)or(pos('|',edit_programs.Text)<>0)or(pos('/',edit_programs.Text)<>0)
        or(pos('*',edit_programs.Text)<>0)or(pos('?',edit_programs.Text)<>0))and((edit_programs.Text<>Form_main.strAlarm)or(edit_programs.Text<>Form_main.strMessage)
        or(edit_programs.Text<>Form_main.strShut)or(edit_programs.Text<>Form_main.strReboot))
  then
    begin
    if (not radiobutton_capt.Checked)and(not radiobutton_class.Checked)
      then
        begin
        s:=edit_programs.Text;
        radiobutton_capt.Checked:=true;
        edit_programs.Text:=s;
        edit_programs.Selstart:=length(edit_programs.text);
        end;
    end;

end;

procedure TForm_edit.RadioButton_killClick(Sender: TObject);
var s:string;
begin
if file2 or capt2 or(pos('<',edit_programs.Text)=1)
  then
  begin
  prog2:=true;
  file2:=false;
  capt2:=false;
  s:='';
  edit_programs.Text:='';
  end
  else s:=edit_programs.Text;
edit_programs.Text:=s;
edit_programs.Selstart:=length(edit_programs.Text);
edit_programs.CharCase:=ecLowerCase;
end;

procedure TForm_edit.RadioButton_captClick(Sender: TObject);
var s:string;
begin
if file2 or prog2 or(pos('<',edit_programs.Text)=1)
  then
  begin
  prog2:=false;
  file2:=false;
  capt2:=true;
  s:='';
  edit_programs.Text:='';
  end
  else s:=edit_programs.Text;
edit_programs.Text:=s;
edit_programs.Selstart:=length(edit_programs.Text);
edit_programs.CharCase:=ecnormal;
if (edit_programs.Text='')or(edit_programs.Text='""')or(edit_programs.Text[1]<>'"')
    then
    begin
    edit_programs.Text:='""';
    edit_programs.Selstart:=1;
    end
    else edit_programs.Selstart:=length(edit_programs.Text)-1;
end;

procedure TForm_edit.RadioButton_fileClick(Sender: TObject);
var s:string;
begin
if prog2 or capt2 or(pos('<',edit_programs.Text)=1)
  then
  begin
  prog2:=false;
  file2:=true;
  capt2:=false;
  s:='';
  edit_programs.Text:='';
  end
  else s:=edit_programs.Text;
edit_programs.Text:=s;
edit_programs.Selstart:=length(edit_programs.Text);
edit_programs.CharCase:=ecLowerCase;
end;

procedure TForm_edit.MaskEdit_timeChange(Sender: TObject);
var t:integer;
begin
t:=maskedit_time.SelStart-1;
if MaskEdit_time.text[1]>'2'
  then
  begin
  MaskEdit_time.text:='2'+copy(edit_time_temp.text,2,7);
  maskedit_time.SelStart:=t+1;
  end;
if ((MaskEdit_time.text[1]='2')and(MaskEdit_time.text[2]>'3'))
  then
  begin
  MaskEdit_time.text:='23'+copy(edit_time_temp.text,3,6);
  maskedit_time.SelStart:=t+1;
  end;
if MaskEdit_time.text[4]>'5'
  then
  begin
  MaskEdit_time.text:=copy(edit_time_temp.text,1,3)+'5'+copy(edit_time_temp.text,5,5);
  maskedit_time.SelStart:=t+1;
  end;
if MaskEdit_time.text[7]>'5'
  then
  begin
  MaskEdit_time.text:=copy(edit_time_temp.text,1,7)+'5'+copy(edit_time_temp.text,8,1);
  maskedit_time.SelStart:=t+1;
  end;
edit_time_temp.text:=maskedit_time.Text;
end;

procedure TForm_edit.SpinEdit_valMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if shift=[ssmiddle] then spinedit_val.value:=spinedit_val.MinValue;
end;

procedure TForm_edit.MaskEdit_timeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case x of
  1..7: maskedit_time.SelStart:=0;
  8..14: maskedit_time.SelStart:=1;
  15..16: maskedit_time.SelStart:=2;
  17..23: maskedit_time.SelStart:=3;
  24..30: maskedit_time.SelStart:=4;
  31..32: maskedit_time.SelStart:=5;
  33..39: maskedit_time.SelStart:=6;
  40..56: maskedit_time.SelStart:=7;
end;
if maskedit_time.SelLength>1
  then maskedit_time.SelStart:=0;
end;

procedure TForm_edit.MaskEdit_timeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=46 then key:=0;
if key=8 then key:=37;
end;

procedure TForm_edit.SpeedButton1Click(Sender: TObject);
begin
edit_programs.Text:=Form_main.strAlarm;
edit_programs.SetFocus;
end;

procedure TForm_edit.SpeedButton2Click(Sender: TObject);
begin
edit_programs.Text:=Form_main.strMessage;
edit_programs.SetFocus;
end;

procedure TForm_edit.SpeedButton3Click(Sender: TObject);
begin
edit_programs.Text:=Form_main.strReboot;
edit_programs.SetFocus;
end;

procedure TForm_edit.SpeedButton4Click(Sender: TObject);
begin
edit_programs.Text:=Form_main.strShut;
edit_programs.SetFocus;
end;

procedure TForm_edit.RadioButton_captMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
edit_programs.SetFocus;
if (edit_programs.Text='')or(edit_programs.Text='""')or(pos('<',edit_programs.Text)=1)
    then
    begin
    edit_programs.Text:='""';
    edit_programs.Selstart:=1;
    end
    else edit_programs.Selstart:=length(edit_programs.Text)-1;
end;

procedure TForm_edit.RadioButton_killMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
edit_programs.SetFocus;
edit_programs.Selstart:=length(edit_programs.Text);
if pos('<',edit_programs.Text)<>0
  then edit_programs.Text:='';
end;



procedure TForm_edit.Hour;
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(hour);
if hour>=24 then
  begin
  hour:=0;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_edit.Min;
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(min);
if min>=60 then
  begin
  min:=0;
  inc(hour);
  if hour>=24 then
    begin
    hour:=0;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_edit.Sec;
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(sec);
if sec>=60 then
  begin
  sec:=0;
  inc(min);
  if min>=60 then
    begin
    min:=0;
    inc(hour);
    if hour>=24 then
      begin
      hour:=0;
      end;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_edit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key of
  113: Hour;
  114: Min;
  115: Sec;
  112: begin
          datetimepicker_date.Date:=date;
          if length(timetostr(time))=7
            then maskedit_time.Text:='0'+timetostr(time)
            else maskedit_time.text:=timetostr(time);
          end;
  end;
end;

end.
