unit uRadioButtons;        //radio-buttons

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, Buttons;

type
  TForm_max1 = class(TForm)
    Panel1: TPanel;
    SpinEdit_val: TSpinEdit;
    RadioButton_file: TRadioButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    RadioButton_go: TRadioButton;
    RadioButton_kill: TRadioButton;
    RadioButton_capt: TRadioButton;
    RadioButton_class: TRadioButton;
    SpeedButton1: TSpeedButton;
    procedure RadioButton_killMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_captMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button_helClick(Sender: TObject);
    procedure SpinEdit_valChange(Sender: TObject);
    procedure RadioButton_killClick(Sender: TObject);
    procedure RadioButton_captClick(Sender: TObject);
    procedure SpinEdit_valMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button_helMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RadioButton_fileClick(Sender: TObject);
    procedure RadioButton_fileMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SpeedButton1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
  prog,file1,capt:boolean;
  tray_button6:string;        //������
  procedure WriteItems(ComboBoxItems :TStrings; Section:string);
    { Public declarations }
  end;

var
  Form_max1: TForm_max1;

implementation

uses uMain, uHelp;

{$R *.dfm}

procedure TForm_max1.WriteItems(ComboBoxItems :TStrings; Section:string);
label 10;
var t,temp:textFile;
    s:string;
    i:integer;
begin
assignfile(t,Form_main.CloserFile);
assignfile(temp,Form_main.TempFile);
reset(t);
rewrite(temp);

while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  if s=Section then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 10;
      end;
    end;
  end;
10:
closefile(temp);
append(temp);
for i:=0 to ComboBoxItems.Count-1 do
  begin
  writeln(temp,ComboBoxItems[i]);
  end;

writeln(temp,s);
while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  end;

closefile(temp);
closeFile(t);
Erase(t);
RenameFile(Form_main.TempFile,Form_main.CloserFile);
end;

procedure TForm_max1.RadioButton_killMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Form_main.combobox_prog.SetFocus;
Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);
if (pos('<',Form_main.combobox_prog.Text)<>0)or(pos('"',Form_main.combobox_prog.Text)<>0)
  then Form_main.combobox_prog.Text:='';
end;

procedure TForm_max1.RadioButton_captMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Form_main.combobox_prog.SetFocus;
if (Form_main.combobox_prog.Text='')or(Form_main.combobox_prog.Text='""')or(pos('<',Form_main.combobox_prog.Text)=1)
    then
    begin
    Form_main.combobox_prog.Text:='""';
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
end;

procedure TForm_max1.RadioButton_fileMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Form_main.combobox_prog.SetFocus;
Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);
if (pos('<',Form_main.combobox_prog.Text)<>0)
        or(pos('"',Form_main.combobox_prog.Text)<>0)
  then Form_main.combobox_prog.Text:='';
end;

procedure TForm_max1.Button_helClick(Sender: TObject);
begin
form_help2.ShowModal;
end;

procedure TForm_max1.SpinEdit_valChange(Sender: TObject);
begin
Form_main.SetFocus;
if (radiobutton_capt.Checked)or(radiobutton_class.Checked)
  then
  begin
  if Form_main.combobox_prog.Text='""'
    then
    begin
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
  end
  else
  if (radiobutton_kill.Checked)or(radiobutton_go.Checked)
    then Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);

end;

procedure TForm_max1.RadioButton_killClick(Sender: TObject);
var s,st:string;
    t:textfile;
begin
if not prog then
  begin
  if capt
    then WriteItems(Form_main.combobox_prog.items,'[closer captions]');
  if file1
  then WriteItems(Form_main.combobox_prog.items,'[closer files]');
  s:='';
  Form_main.combobox_prog.CharCase:=ecLowerCase;
  Form_main.combobox_prog.Clear;

  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[closer programs]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        Form_main.combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);

  end
  else s:=Form_main.combobox_prog.Text;
prog:=true;
capt:=false;
file1:=false;
SpinEdit_val.Value:=1;
Form_main.combobox_prog.Text:=s;
Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);
end;

procedure TForm_max1.RadioButton_captClick(Sender: TObject);
var s,st:string;
    t:textFile;
begin
if not capt then
  begin
  if prog
    then WriteItems(Form_main.combobox_prog.items,'[closer programs]');
  if file1
    then WriteItems(Form_main.combobox_prog.items,'[closer files]');
  if Form_main.combobox_prog.Text=''
    then
    begin
    Form_main.combobox_prog.Text:='';
    Form_main.combobox_prog.Selstart:=2;
    end;
  Form_main.combobox_prog.CharCase:=ecnormal;
  Form_main.combobox_prog.Clear;

  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[closer captions]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        Form_main.combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);

  end
  else s:=Form_main.combobox_prog.Text;
prog:=false;
capt:=true;
file1:=false;
SpinEdit_val.Value:=1;
Form_main.combobox_prog.Text:=s;
if (Form_main.combobox_prog.Text='')or(Form_main.combobox_prog.Text='""')
        or(pos('<',Form_main.combobox_prog.Text)<>0)
    then
    begin
    Form_main.combobox_prog.Text:='""';
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
end;

procedure TForm_max1.RadioButton_fileClick(Sender: TObject);
var //i,ii:integer;
    s,st:string;
    t:TextFile;
begin
if not file1 then
  begin
  if prog
    then WriteItems(Form_main.combobox_prog.items,'[closer programs]');
  if capt
    then WriteItems(Form_main.combobox_prog.items,'[closer captions]');
  s:='';
  Form_main.combobox_prog.CharCase:=ecLowerCase;
  Form_main.combobox_prog.Clear;

  assignfile(t,Form_main.CloserFile);
  reset(t);
  while not eof(t) do
    begin
    readln(t,st);
    if st='[closer files]' then
      begin
      while not eof(t) do
        begin
        readln(t,st);
        if (st='')or(st[1]='[')
          then break;
        Form_main.combobox_prog.Items.Add(st);
        end;
      end;
    end;
  CloseFile(t);
  end
  else s:=Form_main.combobox_prog.Text;
prog:=false;
capt:=false;
file1:=true;
SpinEdit_val.Value:=1;
Form_main.combobox_prog.Text:=s;
Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);
end;

procedure TForm_max1.SpinEdit_valMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if shift=[ssmiddle] then spinedit_val.value:=spinedit_val.MinValue;
end;

procedure TForm_max1.Button_helMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button6;
end;

procedure TForm_max1.FormCreate(Sender: TObject);
begin
form_max1.left:=Form_main.Left+Form_main.Width;
form_max1.Top:=Form_main.Top+53;
prog:=true;
file1:=false;
capt:=false;
end;

procedure TForm_max1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.SetFocus;
if (radiobutton_capt.Checked)or(radiobutton_class.Checked)
  then
  begin
  if Form_main.combobox_prog.Text='""'
    then
    begin
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
  end
  else
  if (radiobutton_kill.Checked)or(radiobutton_go.Checked)or(radiobutton_file.Checked)
    then Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);

end;

procedure TForm_max1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Form_main.statusbar1.panels[0].text:='';
end;

procedure TForm_max1.SpeedButton1Click(Sender: TObject);
begin
//form_help2.ShowModal;
form_main.n29.Click;
end;

procedure TForm_max1.SpeedButton1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
Form_main.statusbar1.panels[0].text:=tray_button6;
end;

procedure TForm_max1.SpeedButton1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Form_main.SetFocus;
if (radiobutton_capt.Checked)or(radiobutton_class.Checked)
  then
  begin
  if Form_main.combobox_prog.Text='""'
    then
    begin
    Form_main.combobox_prog.Selstart:=1;
    end
    else Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text)-1;
  end
  else
  if (radiobutton_kill.Checked)or(radiobutton_go.Checked)or(radiobutton_file.Checked)
    then Form_main.combobox_prog.Selstart:=length(Form_main.combobox_prog.Text);

end;

end.
