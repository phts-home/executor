unit uMain;        //EXECUTOR

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Grids, Mask, Spin, ExtCtrls, Menus, Tlhelp32,
  MPlayer, ImgList, Buttons, printers, shellAPI, IniFiles;

const
 WM_MYICONNOTIFY = WM_USER + 123;

type
  TForm_main = class(TForm)
    listbox_programs: TListBox;
    OpenDialog_addexe: TOpenDialog;
    MaskEdit_time: TMaskEdit;
    DateTimePicker_date: TDateTimePicker;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    exit: TMenuItem;
    save: TMenuItem;
    open: TMenuItem;
    new: TMenuItem;
    N2: TMenuItem;
    current: TMenuItem;
    N4: TMenuItem;
    abprog: TMenuItem;
    exwin: TMenuItem;
    N3: TMenuItem;
    add: TMenuItem;
    SaveDialog_save: TSaveDialog;
    OpenDialog_open: TOpenDialog;
    ComboBox_prog: TComboBox;
    MediaPlayer1: TMediaPlayer;
    bud: TMenuItem;
    soob: TMenuItem;
    opt: TMenuItem;
    N8: TMenuItem;
    reboot: TMenuItem;
    N10: TMenuItem;
    Timer2: TTimer;
    print: TMenuItem;
    ListBox_temp2: TListBox;
    Timer3: TTimer;
    N6: TMenuItem;
    N11: TMenuItem;
    N7: TMenuItem;
    N5: TMenuItem;
    N9: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    OpenDialog_addfile: TOpenDialog;
    ImageList1: TImageList;
    PopupMenu1: TPopupMenu;
    CloseItem: TMenuItem;
    RestoreItem: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N27: TMenuItem;
    N26: TMenuItem;
    N28: TMenuItem;
    PrintAll: TMenuItem;
    PrintTimer: TMenuItem;
    N30: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    Timer4: TTimer;
    ReadMe1: TMenuItem;
    N29: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    N51: TMenuItem;
    N36: TMenuItem;
    N52: TMenuItem;
    N110: TMenuItem;
    N111: TMenuItem;
    N37: TMenuItem;
    N112: TMenuItem;
    N113: TMenuItem;
    N114: TMenuItem;
    N38: TMenuItem;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    ImgLst_max1: TImageList;
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    N39: TMenuItem;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure saveClick(Sender: TObject);
    procedure exitClick(Sender: TObject);
    procedure openClick(Sender: TObject);
    procedure newClick(Sender: TObject);
    procedure currentClick(Sender: TObject);
    procedure exwinClick(Sender: TObject);
    procedure listbox_programsMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure abprogClick(Sender: TObject);
    procedure budClick(Sender: TObject);
    procedure soobClick(Sender: TObject);
    procedure optClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rebootClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure ComboBox_progChange(Sender: TObject);
    procedure MaskEdit_timeChange(Sender: TObject);
    procedure printClick(Sender: TObject);
    procedure MaskEdit_timeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MaskEdit_timeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button_timerClick(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure CloseItemClick(Sender: TObject);
    procedure RestoreItemClick(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure PrintAllClick(Sender: TObject);
    procedure PrintTimerClick(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure Timer4Timer(Sender: TObject);
    procedure ReadMe1Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure N110Click(Sender: TObject);
    procedure N52Click(Sender: TObject);
    procedure N111Click(Sender: TObject);
    procedure N114Click(Sender: TObject);
    procedure N113Click(Sender: TObject);
    procedure N112Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn5MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn6MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BitBtn4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);



  private
    ShownOnce: Boolean;
    { Private declarations }
  public
  tray_listbox1,              //������ �������
  tray_button1,               //�������� �������
  tray_button2,               //������� ���������� �����
  tray_button3,               //��������� ���������� �����
  tray_button4,               //������
  tray_button5,               //�������� ����� ��������� / ����
        ErrorMessage1,        //������ ��� ������ ����� ��� ���� �� ������
        ErrorMessage2,        //������������ ��������.
        ErrorMessage3,        //������. �� ������ �����������
//        ErrorMessage4,        //�� ����� ������ �����������?
        ErrorMessage5,        //���� �������� ��� ����� ����������� ������
        ErrorMessage6,        //�� ����� ������ ������� ��� ����������� ���������?
        ErrorMessage7,        //������������� ��� *.xxc � ����������?
        strPrn,               //��������� ��� ������    ������
        strAlarm,strMessage,strShut,strReboot,    //<���������> � ��.
        strShuting,strRebooting: String;           //��������� form_exwin
  CloserFile, OpeningFile, TempFile: String;
  ShowRdBtns: Boolean;
  Ini,IniOpen: TIniFile;
  Bmax1: TBitMap;
    procedure CreateExecutorData;
    procedure CheckHorizontalBar (ListBox: TListBox);
    procedure ReadXXCFile (FilePath:string);
    procedure WriteXXCFile(FilePath:string);
    procedure ReadIniFile (FilePath:string);
    procedure WriteIniFile(FilePath:string);
//    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    /////*/**/*/*/*/*/*/*/*
    procedure WMICON(var msg: TMessage); message WM_MYICONNOTIFY;
    procedure WMSYSCOMMAND(var msg: TMessage);message WM_SYSCOMMAND;
    procedure RestoreMainForm;
    procedure HideMainForm;
    procedure CreateTrayIcon;
    procedure DeleteTrayIcon;

    { Public declarations }
  end;

var
  Form_main: TForm_main;
  ShowTimer: Boolean;


implementation

uses uAbout, uEdit, uFolder, uHelp, uOptions, uRadioButtons, uShutDown, uStopButton, uTimer,
  uSplash;


{$R *.dfm}

procedure TForm_main.CreateExecutorData;
var f1:textfile;
    i:word;
    s:string;
begin
CreateDir(extractfilepath(paramstr(0))+'Data\');
assignfile(f1,Form_main.CloserFile);
rewrite(f1);
for i:=0 to Form_main.ListBox_temp2.Items.Count-1 do
  begin
  s:=Form_main.ListBox_temp2.Items[i];
  if s='language='
    then s:='language='+extractfilepath(paramstr(0))+'Data\rus.lng';
  if s='scheme='
    then s:='language='+extractfilepath(paramstr(0))+'Data\standart.col';
  if s='directory='
    then s:='directory='+extractfilepath(paramstr(0))+'Lists\';
  if s='music='
    then
    begin
    if fileExists(extractfilepath(paramstr(0))+'music\Metallica - Master Of Puppets (Master Of Puppets).mid')
      then s:='music='+extractfilepath(paramstr(0))+'music\Metallica - Master Of Puppets (Master Of Puppets).mid'
      else s:='music=c:\windows\media\tada.wav';
    end;
  if s='mmusic='
    then
    begin
    if fileExists(extractfilepath(paramstr(0))+'music\Metallica - Master Of Puppets (Master Of Puppets).mid')
      then s:='mmusic='+extractfilepath(paramstr(0))+'music\Metallica - Master Of Puppets (Master Of Puppets).mid'
      else s:='mmusic=c:\windows\media\tada.wav';
    end;
  writeln(f1,s);
  end;
closefile(f1);
end;

procedure TForm_main.CheckHorizontalBar (ListBox: TListBox);
var i,max:integer;
begin
max:=0;
for i:=0 to ListBox.items.count-1 do
  begin
  if length(ListBox.items[i])>max
    then max:=length(ListBox.items[i]);
  end;
sendmessage(ListBox.Handle,lb_sethorizontalextent,round(max*5.1),0);
end;

procedure TForm_main.Timer1Timer(Sender: TObject);
var i,l:integer;
    h:hwnd;
    buffer: array [0..255] of char;
begin
if length(timetostr(time))=7
  then
    StatusBar1.Panels[3].Text:=datetostr(date)+'    '+timetostr(time)
  else
    StatusBar1.Panels[3].Text:=datetostr(date)+'   '+timetostr(time);

for i:=0 to listbox_programs.Count-1 do
  if (copy(listbox_programs.items[i],0,21)=StatusBar1.Panels[3].Text)
    then
      begin
      if (pos('<',listbox_programs.items[i])=0) and (copy(listbox_programs.items[i],24,3)<>'(=)') and
                      (copy(listbox_programs.items[i],24,3)<>'(#)')
        then
          begin
          if copy(listbox_programs.items[i],24,3)='(+)'
            then
              begin
              for l:=1to strtoint(copy(listbox_programs.Items[i],length(listbox_programs.Items[i])-1,1)) do
                  winexec(pchar(copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-6-28))),1);
              end;
          if copy(listbox_programs.items[i],24,3)='(x)'
            then
              begin
              for l:=1to strtoint(copy(listbox_programs.Items[i],length(listbox_programs.Items[i])-1,1)) do
                  winexec(pchar('taskkill /f /im '+ExtractFileName(copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-6-28)))),0);
              end;
          if copy(listbox_programs.items[i],24,3)='(F)'
            then
              begin
              for l:=1to strtoint(copy(listbox_programs.Items[i],length(listbox_programs.Items[i])-1,1)) do
                begin
                GetCurrentDirectory(sizeof(buffer),buffer);
                shellexecute(0,nil,pchar(copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-6-28))),nil,buffer,SW_SHOW);
                end;
              end;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-28)));
          listbox_programs.items.delete(i);
          end;

      if copy(listbox_programs.items[i],24,3)='(=)'
        then
          begin
          for l:=1to strtoint(copy(listbox_programs.Items[i],length(listbox_programs.Items[i])-1,1)) do
            begin
            h:=findwindow(nil,pchar(copy(listbox_programs.items[i],30,(length(listbox_programs.items[i])-6-30))));
            sendmessage(h,wm_close,0,0);
            end;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-28)));
          listbox_programs.items.delete(i);
          end;

      if copy(listbox_programs.items[i],24,3)='(#)'
        then
          begin
          for l:=1to strtoint(copy(listbox_programs.Items[i],length(listbox_programs.Items[i])-1,1)) do
            begin
            h:=findwindow(pchar(copy(listbox_programs.items[i],30,(length(listbox_programs.items[i])-6-30))),nil);
            sendmessage(h,wm_close,0,0);
            end;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+copy(listbox_programs.items[i],29,(length(listbox_programs.items[i])-28)));
          listbox_programs.items.delete(i);
          end;

      if pos(strShut,listbox_programs.items[i])=29
        then
          begin
          form_exwin.Close;
          if Form_opt.CheckBox_timer.Checked=true
            then form_exwin.show;
          form_exwin.Label1.Caption:=strShuting;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+strShut);
          listbox_programs.items.delete(i);
          end;

      if pos(strReboot,listbox_programs.items[i])=29
        then
          begin
          form_exwin.Close;
          form_exwin.show;
          form_exwin.Label1.Caption:=strRebooting;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+strReboot);
          listbox_programs.items.delete(i);
          end;

      if pos(strAlarm,listbox_programs.items[i])=29
        then
          begin
          try begin
          with MediaPlayer1 do
            begin
            Close;
            FileName:=form_opt.edit_bud.text;
            Open;
            Play;
            end;
          form_play.show;
          end;
          except
            begin
            Application.MessageBox(pchar(ErrorMessage1+#13+form_opt.edit_bud.text),pchar(Form_main.Caption),MB_ICONERROR+mb_ok);
//            form_opt.showmodal;
            end;
          end;
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+strAlarm);
          listbox_programs.items.delete(i);
          end;

      if pos(strMessage,listbox_programs.items[i])=29
        then
          begin
          if form_opt.checkbox_mus.checked=true
            then
              try begin
              with MediaPlayer1 do
                begin
                Close;
                FileName:=form_opt.edit_soob_mus.text;
                Open;
                Play;
                end;
              form_play.show;
              application.MessageBox(pchar(form_opt.edit_soob.text),pchar(Form_main.Caption),MB_IconWarning+MB_OK);
              end;
              except
                begin
                Application.MessageBox(pchar(ErrorMessage1+#13+form_opt.edit_soob_mus.text),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
//                form_opt.showmodal;
                end;
              end
            else
              application.MessageBox(pchar(form_opt.edit_soob.text),pchar(Form_main.Caption),MB_IconWarning+MB_OK);
          listbox_programs.items.add(copy(listbox_programs.items[i],0,21)+'  ***  '+strMessage);
          listbox_programs.items.delete(i);
          end;
      end;
if n31.Checked
  then
  begin
  i:=0;
  while i<=listbox_programs.Count-1 do
    begin
    if copy(listbox_programs.Items[i],24,3)='***'
      then listbox_programs.Items.Delete(i);
    inc(i);
    end;
  end;

StatusBar1.Panels[1].Text:=inttostr(listbox_programs.Count);
StatusBar1.Panels[2].Text:=inttostr(form_max2.ListBox_prog.Count);
end;

procedure TForm_main.FormCreate(Sender: TObject);
begin
CloserFile:=extractfilepath(paramstr(0))+'data\Executor2_1.ini';
TempFile:=extractfilepath(paramstr(0))+'data\Temp.tmp';
datetimepicker_date.Date:=date;
if length(timetostr(time))=7
  then maskedit_time.Text:='0'+timetostr(time)
  else maskedit_time.text:=timetostr(time);
ChDir(extractfilepath(paramstr(0))+'Data\');
Ini:=TIniFile.Create(CloserFile);

ErrorMessage1:='������ ��� ������ ����� ��� ���� �� ������';

n15.Checked:=ini.ReadBool('program','savesize',true);
n16.Checked:= ini.ReadBool('program','savepos',true);
width:=ini.ReadInteger('program','width',443);
height:=ini.ReadInteger('program','height',237);
Left:=ini.ReadInteger('program','left',320);
Top:=ini.ReadInteger('program','top',264);
if n15.Checked
  then
  begin
  Form_main.Width:=width;
  Form_main.Height:=Height;
  N15.ImageIndex:=19;
  end
  else
  begin
  Form_main.Width:=443;
  Form_main.Height:=237;
  N15.ImageIndex:=-1;
  end;
if n16.Checked
  then
  begin
  Form_main.left:=left;
  Form_main.top:=top;
  N16.ImageIndex:=19;
  end
  else
  begin
  Form_main.left:=320;
  Form_main.top:=264;
  N16.ImageIndex:=-1;
  end;
end;

procedure TForm_main.saveClick(Sender: TObject);
begin
savedialog_save.initialDir:=form_opt.Edit_dir.Text;
if savedialog_save.execute then WriteXXCFile(savedialog_save.filename);
end;

procedure TForm_main.exitClick(Sender: TObject);
begin
Form_main.Close;
end;

procedure TForm_main.openClick(Sender: TObject);
begin
opendialog_open.InitialDir:=form_opt.Edit_dir.Text;
if opendialog_open.Execute
  then
  begin
  ReadXXCFile(opendialog_open.filename);
  Form_opt.ChangeLanguage(Form_opt.CurrentLanguage);
  OpenDialog_open.FileName:='';
  if form_max2.ListBox_prog.Items.Count<>0 then form_max2.Timer1.Enabled:=true;
  end;
end;

procedure TForm_main.newClick(Sender: TObject);
begin
listbox_programs.Items.Clear;
CheckHorizontalBar(Form_main.listbox_programs);
end;

procedure TForm_main.currentClick(Sender: TObject);
begin
datetimepicker_date.Date:=date;
if length(timetostr(time))=7
  then maskedit_time.Text:='0'+timetostr(time)
  else maskedit_time.text:=timetostr(time);
end;

procedure TForm_main.abprogClick(Sender: TObject);
begin
form_help.ShowModal;
end;

procedure TForm_main.listbox_programsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_listbox1;
end;

procedure TForm_main.FormMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
statusbar1.panels[0].text:='';
end;

procedure TForm_main.Panel1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
statusbar1.panels[0].text:='';
end;

procedure TForm_main.budClick(Sender: TObject);
begin
ComboBox_prog.Text:=strAlarm;
end;

procedure TForm_main.soobClick(Sender: TObject);
begin
ComboBox_prog.Text:=strMessage;
end;

procedure TForm_main.exwinClick(Sender: TObject);
begin
ComboBox_prog.Text:=strShut;
end;

procedure TForm_main.rebootClick(Sender: TObject);
begin
ComboBox_prog.Text:=strReboot;
end;

procedure TForm_main.optClick(Sender: TObject);
begin
form_opt.showmodal;
end;

procedure TForm_main.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
WriteIniFile(CloserFile);
end;

procedure TForm_main.FormShow(Sender: TObject);
begin
combobox_prog.SetFocus;
end;

procedure TForm_main.Timer2Timer(Sender: TObject);
begin
if ShowRdBtns
  then
    begin
    if form_max1.Width<=6
        then
          begin
          Bmax1:=TBitMap.Create;
          ImgLst_max1.GetBitmap(1,Bmax1);
          BitBtn6.Glyph:=Bmax1;
          ShowRdBtns:=false;
          timer2.Enabled:=false;
          form_max1.Visible:=false;
          end
        else
          form_max1.Width:=form_max1.Width-6;
    end
  else
    begin
    if form_max1.Width>=114
      then
        begin
        timer2.Enabled:=false;
        Bmax1:=TBitMap.Create;
        ImgLst_max1.GetBitmap(0,Bmax1);
        BitBtn6.Glyph:=Bmax1;
        ShowRdBtns:=true;
        end
      else
        form_max1.Width:=form_max1.Width+6;
    end;
end;

procedure TForm_main.ComboBox_progChange(Sender: TObject);
var s:string;
begin
if (pos('.exe',ComboBox_prog.Text)>4)and(pos(':\',ComboBox_prog.Text)=2)and(pos('"',ComboBox_prog.Text)=0)
  then
    begin
    if (not Form_max1.radiobutton_go.Checked)and(not Form_max1.radiobutton_kill.Checked)
      then
        begin
        s:=combobox_prog.Text;
        Form_max1.radiobutton_go.Checked:=true;
        combobox_prog.Text:=s;
        combobox_prog.Selstart:=length(combobox_prog.text);
        end;
    end;

if ((pos('"',ComboBox_prog.Text)<>0)or(pos('>',combobox_prog.Text)<>0)or(pos('|',combobox_prog.Text)<>0)or(pos('/',combobox_prog.Text)<>0)
        or(pos('*',combobox_prog.Text)<>0)or(pos('?',combobox_prog.Text)<>0))and(combobox_prog.Text<>'*�����*')
  then
    begin
    if (not Form_max1.radiobutton_capt.Checked)and(not Form_max1.radiobutton_class.Checked)
      then
        begin
        s:=combobox_prog.Text;
        Form_max1.radiobutton_capt.Checked:=true;
        combobox_prog.Text:=s;
        combobox_prog.Selstart:=length(combobox_prog.text);
        end;
    end;
end;

procedure TForm_main.MaskEdit_timeChange(Sender: TObject);
var t:integer;
begin
t:=maskedit_time.SelStart-1;
if MaskEdit_time.text[1]>'2'
  then
  begin
  MaskEdit_time.text:='2'+copy(MaskEdit_time.text,2,7);
  maskedit_time.SelStart:=t+1;
  end;
if ((MaskEdit_time.text[1]='2')and(MaskEdit_time.text[2]>'3'))
  then
  begin
  MaskEdit_time.text:='23'+copy(MaskEdit_time.text,3,6);
  maskedit_time.SelStart:=t+1;
  end;
if MaskEdit_time.text[4]>'5'
  then
  begin
  MaskEdit_time.text:=copy(MaskEdit_time.text,1,3)+'5'+copy(MaskEdit_time.text,5,5);
  maskedit_time.SelStart:=t+1;
  end;
if MaskEdit_time.text[7]>'5'
  then
  begin
  MaskEdit_time.text:=copy(MaskEdit_time.text,1,7)+'5'+copy(MaskEdit_time.text,8,1);
  maskedit_time.SelStart:=t+1;
  end;
end;

procedure PrintStrings(Strings1, Strings2: TStrings);
var Prn: TextFile;
    i: word;
begin
AssignPrn(Prn);
try
  Rewrite(Prn);
  try
    if Strings1<>nil
      then
      begin
      writeln(prn,'');
      writeln(prn,'   [ '+Form_main.caption+' ]');
      for i := 0 to Strings1.Count - 1 do
        writeln(Prn,'   '+ Strings1.Strings[i]);
      end;
    if Strings2<>nil
      then
      begin
      writeln(prn,'');
      writeln(prn,'   [ '+Form_main.caption+' ( '+Form_main.strPrn+' ) ]');
      for i := 0 to Strings2.Count - 1 do
        writeln(Prn,'   '+ Strings2.Strings[i]);
      end;
  finally
    CloseFile(Prn);
  end;
except
  on EInOutError do
    Application.MessageBox(pchar(Form_main.ErrorMessage3),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
end;
end;

procedure TForm_main.printClick(Sender: TObject);
begin
//if application.MessageBox(pchar(Form_main.ErrorMessage4),pchar(Form_main.caption),MB_ICONQUESTION+MB_YESNO)=6
if PrinterSetupDialog1.Execute then
  if PrintDialog1.Execute
    then PrintStrings(Listbox_programs.Items, nil);
end;

procedure TForm_main.MaskEdit_timeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case x of
  1..7: maskedit_time.SelStart:=0;
  8..14: maskedit_time.SelStart:=1;
  15..16: maskedit_time.SelStart:=2;
  17..23: maskedit_time.SelStart:=3;
  24..30: maskedit_time.SelStart:=4;
  31..32: maskedit_time.SelStart:=5;
  33..39: maskedit_time.SelStart:=6;
  40..56: maskedit_time.SelStart:=7;
  end;
end;

procedure TForm_main.MaskEdit_timeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=46 then key:=0;
if key=8 then key:=37;
end;

procedure TForm_main.Button_timerClick(Sender: TObject);
begin
if not ShowTimer
  then
    begin
    form_max2.Visible:=true;
    end;
timer3.Enabled:=true;
combobox_prog.SetFocus;
end;

procedure TForm_main.Timer3Timer(Sender: TObject);
begin
if ShowTimer
  then
    begin
    if form_max2.Height<=6
        then
          begin
          timer3.Enabled:=false;
          ShowTimer:=false;
          form_max2.Visible:=false;
          end
        else
          form_max2.Height:=form_max2.Height-6;
    end
  else
    begin
    if form_max2.Height>=132
      then
        begin
        timer3.Enabled:=false;
        ShowTimer:=true;
        end
      else
        form_max2.Height:=form_max2.Height+6;
    end;
end;

procedure TForm_main.N11Click(Sender: TObject);
begin
form_max2.listbox_prog.Items.Clear;
form_max2.combobox_prog.setfocus;
form_max2.timer1.Enabled:=false;

form_max2.b:=TBitMap.Create;
form_max2.num:=0;
form_max2.ImgLst_clock.GetBitmap(form_max2.num,form_max2.b);
Form_main.BitBtn4.Glyph:=form_max2.b;
CheckHorizontalBar(form_max2.listbox_prog);
end;

procedure TForm_main.N9Click(Sender: TObject);
begin
form_max2.ComboBox_prog.Text:=strMessage;
try form_max2.ComboBox_prog.SetFocus;
  except end;
end;

procedure TForm_main.N12Click(Sender: TObject);
begin
form_max2.ComboBox_prog.Text:=strReboot;
try form_max2.ComboBox_prog.SetFocus;
  except end;
end;

procedure TForm_main.N13Click(Sender: TObject);
begin
form_max2.ComboBox_prog.Text:=strShut;
try form_max2.ComboBox_prog.SetFocus;
  except end;
end;

procedure TForm_main.FormResize(Sender: TObject);
begin
listbox_programs.Width:=Form_main.Width-102;
listbox_programs.Height:=Form_main.Height-116;
BitBtn1.left:=Form_main.Width-93;
BitBtn2.left:=Form_main.Width-93;
BitBtn3.left:=Form_main.Width-93;
BitBtn4.left:=Form_main.Width-93;
BitBtn6.left:=Form_main.Width-51;
BitBtn5.Left:=Form_main.Width-93;
statusbar1.top:=Form_main.ClientHeight-19;
statusbar1.panels[0].Width:=Form_main.Width-180;
statusbar1.Width:=Form_main.Width;
DateTimePicker_date.Top:=Form_main.Height-101;
MaskEdit_time.Top:=Form_main.Height-101;
ComboBox_prog.Top:=Form_main.Height-101;
BitBtn5.Top:=Form_main.Height-105;
ComboBox_prog.Width:=Form_main.Width-252;

end;

procedure TForm_main.N14Click(Sender: TObject);
begin
Form_main.Width:=443;
Form_main.Height:=237;
end;

//======================================================//
//======================================================//

procedure TForm_main.WMICON(var msg: TMessage);
var P : TPoint;
begin
 case msg.LParam of
 WM_RBUTTONDOWN:
  begin
   GetCursorPos(p);
   SetForegroundWindow(Application.MainForm.Handle);
   PopupMenu1.Popup(P.X, P.Y);
  end;
 WM_LBUTTONDBLCLK :
  begin
  SetForegroundWindow(Application.MainForm.Handle);
  RestoreItemClick(Self);
  end;
 end;
end;

procedure TForm_main.WMSYSCOMMAND(var msg: TMessage);
begin
inherited;
if (Msg.wParam=SC_MINIMIZE)and(Form_main.N17.Checked)
  then
  begin
  N20Click(Self);
  end;
end;

procedure TForm_main.HideMainForm;
begin
  Application.ShowMainForm := False;
  ShowWindow(Application.Handle, SW_HIDE);
  ShowWindow(Application.MainForm.Handle, SW_HIDE);
end;

procedure TForm_main.RestoreMainForm;
var i,j : Integer;
begin
try
  Form_help.Close
  except end;
Application.ShowMainForm := True;
ShowWindow(Application.Handle, SW_RESTORE);
ShowWindow(Application.MainForm.Handle, SW_RESTORE);
if not ShownOnce then
  begin
  for I := 0 to Application.MainForm.ComponentCount -1 do
    if Application.MainForm.Components[I] is TWinControl then
      with Application.MainForm.Components[I] as TWinControl do
        if Visible then
          begin
          ShowWindow(Handle, SW_SHOWDEFAULT);
          for J := 0 to ComponentCount -1 do
            if Components[J] is TWinControl then
               ShowWindow((Components[J] as TWinControl).Handle, SW_SHOWDEFAULT);
          end;
  ShownOnce := True;
  end;
end;

procedure TForm_main.CreateTrayIcon;
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
   uFlags := NIF_ICON or NIF_MESSAGE or NIF_TIP;
   uCallBackMessage := WM_MYICONNOTIFY;
   hIcon := Form_main.Icon.Handle;
   StrPCopy(szTip,Application.Title);
  end;
  Shell_NotifyIcon(NIM_ADD, @nidata);
end;

procedure TForm_main.DeleteTrayIcon;
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
  end;
  Shell_NotifyIcon(NIM_DELETE, @nidata);
end;

procedure TForm_main.CloseItemClick(Sender: TObject);
begin
Close;
end;

procedure TForm_main.RestoreItemClick(Sender: TObject);
begin
RestoreMainForm;
DeleteTrayIcon;
end;

procedure TForm_main.N20Click(Sender: TObject);
begin
application.Minimize;
HideMainForm;
CreateTrayIcon;
end;

procedure TForm_main.FormDestroy(Sender: TObject);
begin
DeleteTrayIcon;
end;


{procedure TForm_main.WMNCHitTest(var M: TWMNCHitTest);
begin
  inherited;
  if M.Result = htCaption
    then
    begin
    Form_main.Timer4.Enabled:=true;
    end
    else Form_main.Timer4.Enabled:=false;
end;   }

//======================================================//
//======================================================//

procedure TForm_main.N27Click(Sender: TObject);
begin
Form_max2.BitBtn3Click(Form_max2.BitBtn3);
end;

procedure TForm_main.PrintAllClick(Sender: TObject);
begin
//if application.MessageBox(pchar(Form_main.ErrorMessage4),pchar(Form_main.caption),MB_ICONQUESTION+MB_YESNO)=6
if PrinterSetupDialog1.Execute then
  if PrintDialog1.Execute
    then PrintStrings(Listbox_programs.Items, form_max2.ListBox_prog.Items);
end;

procedure TForm_main.PrintTimerClick(Sender: TObject);
begin
//if application.MessageBox(pchar(Form_main.ErrorMessage4),pchar(Form_main.caption),MB_ICONQUESTION+MB_YESNO)=6
if PrinterSetupDialog1.Execute then
  if PrintDialog1.Execute
    then PrintStrings(nil, form_max2.ListBox_prog.Items);
end;

procedure TForm_main.N19Click(Sender: TObject);
begin
Form_max2.BitBtn4.OnClick(Form_max2.BitBtn4);
end;

procedure TForm_main.Timer4Timer(Sender: TObject);
begin
form_max1.left:=Form_main.Left+Form_main.Width;
form_max1.Top:=Form_main.Top+53;
form_max2.Left:=Form_main.Left;
form_max2.Top:=Form_main.Top+Form_main.Height;
end;

procedure TForm_main.ReadMe1Click(Sender: TObject);
begin
if FileExists(extractfilepath(paramstr(0))+'ReadMe.txt')
  then ShellExecute(0,'',pchar(extractfilepath(paramstr(0))+'ReadMe.txt'),'','',1)
  else Application.MessageBox(pchar(ErrorMessage1+#13+extractfilepath(paramstr(0))+'ReadMe.txt'),pchar(Form_main.Caption),MB_ICONERROR+mb_OK);
end;

procedure TForm_main.N29Click(Sender: TObject);
begin
if FileExists(extractfilepath(paramstr(0))+'Executor Help.chm')
  then ShellExecute(0,'',pchar(extractfilepath(paramstr(0))+'Executor Help.chm'),'','',1)
  else Application.MessageBox(pchar(ErrorMessage1+#13+extractfilepath(paramstr(0))+'Executor Help.chm'),pchar(Form_main.Caption),MB_ICONERROR+mb_OK);
end;

procedure TForm_main.N15Click(Sender: TObject);
begin
if N15.Checked
  then N15.ImageIndex:=19
  else N15.ImageIndex:=-1;
end;

procedure TForm_main.N16Click(Sender: TObject);
begin
if N16.Checked
  then N16.ImageIndex:=19
  else N16.ImageIndex:=-1;
end;

procedure TForm_main.N17Click(Sender: TObject);
begin
if N17.Checked
  then N17.ImageIndex:=19
  else N17.ImageIndex:=-1;
end;

procedure TForm_main.N18Click(Sender: TObject);
begin
if N18.Checked
  then N18.ImageIndex:=19
  else N18.ImageIndex:=-1;
end;

procedure TForm_main.N31Click(Sender: TObject);
begin
if N31.Checked
  then N31.ImageIndex:=19
  else N31.ImageIndex:=-1;
end;

procedure TForm_main.N32Click(Sender: TObject);
begin
if savedialog1.Execute
  then
  begin
  ListBox_temp2.Items.SaveToFile(savedialog1.FileName);
  WriteIniFile(savedialog1.FileName);
  end;
end;

procedure TForm_main.N33Click(Sender: TObject);
begin
if openDialog1.Execute
  then
  begin
  ReadIniFile(openDialog1.FileName);
  end;
end;

procedure TForm_main.ReadIniFile(FilePath: string);
label 10,20,30;
var s:string;
    t:TextFile;
    IniFile:TIniFile;
begin
if (FilePath=CloserFile) and not FileExists(FilePath)
  then CreateExecutorData;
IniFile:=TIniFile.Create(FilePath);
assignfile(t,FilePath);

n15.Checked:=IniFile.ReadBool('program','savesize',true);
n16.Checked:= IniFile.ReadBool('program','savepos',true);
Left:=IniFile.ReadInteger('program','left',320);
Top:=IniFile.ReadInteger('program','top',264);
if n16.Checked
  then
  begin
  Form_main.left:=left;
  Form_main.top:=top;
  N16.ImageIndex:=19;
  end
  else
  begin
  Form_main.left:=320;
  Form_main.top:=264;
  N16.ImageIndex:=-1;
  end;

Form_splash.Left:=Form_main.Left+23;
Form_splash.Top:=Form_main.Top+37;
Form_splash.ProgressBar1.Position:=0;
//Form_splash.Timer1.Enabled:=true;
Form_splash.show;
application.ProcessMessages;

width:=IniFile.ReadInteger('program','width',443);
height:=IniFile.ReadInteger('program','height',237);
N17.Checked:=IniFile.ReadBool('program','autominimize',true);
N18.Checked:=IniFile.ReadBool('program','startminimize',false);
N31.Checked:=IniFile.ReadBool('program','autodelete',false);
/////
Form_splash.ProgressBar1.Position:=10;
/////
if form_main.Ini.ReadBool('program','maxwindow1',true)
  then
      begin
      ShowRdBtns:=true;

      Bmax1:=TBitMap.Create;
      ImgLst_max1.GetBitmap(0,Bmax1);
      BitBtn6.Glyph:=Bmax1;

      form_max1.Visible:=true;
      form_max1.Width:=114;
      end
      else
      begin
      ShowRdBtns:=false;

      Bmax1:=TBitMap.Create;
      ImgLst_max1.GetBitmap(1,Bmax1);
      BitBtn6.Glyph:=Bmax1;
      form_max1.Visible:=false;
      form_max1.Width:=6;
      end;
/////
Form_splash.ProgressBar1.Position:=30;
/////
if IniFile.ReadString('program','maxwindow2','0')='1'
      then
      begin
      showTimer:=true;
      form_max2.Visible:=true;
      form_max2.height:=132;
      end
      else
      begin
      showTimer:=false;
      form_max2.Visible:=false;
      form_max2.height:=6;
      end;
/////
Form_splash.ProgressBar1.Position:=35;
/////
Form_opt.CurrentScheme:=IniFile.ReadString('options','scheme',extractfilepath(paramstr(0))+'Data\standart.col');
if form_opt.ChangeScheme(Form_opt.CurrentScheme)=false
  then
  begin
  if Form_opt.CurrentScheme<>extractfilepath(paramstr(0))+'Data\standart.col'
          then Application.MessageBox(pchar(ErrorMessage1+#13+Form_opt.CurrentScheme),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
  Form_opt.CurrentScheme:=extractfilepath(paramstr(0))+'Data\standart.col';
  if form_opt.ChangeScheme(Form_opt.CurrentScheme)=false
    then
    begin
    form_opt.CreateScheme;
    form_opt.ChangeScheme(Form_opt.CurrentScheme);
    end;
  end;
/////
Form_splash.ProgressBar1.Position:=40;
/////
Form_opt.edit_bud.Text:=IniFile.ReadString('options','music','c:\windows\media\tada.wav');
Form_opt.edit_soob.Text:=IniFile.ReadString('options','message','���������!');
Form_opt.edit_soob_mus.Text:=IniFile.ReadString('options','mmusic','c:\windows\media\tada.wav');
Form_opt.spinedit_timer.Value:=IniFile.ReadInteger('options','timer',10);
Form_opt.edit_dir.Text:=IniFile.ReadString('options','directory',extractfilepath(paramstr(0))+'Lists\');
/////
Form_splash.ProgressBar1.Position:=45;
/////
if Form_opt.edit_dir.Text=''
  then Form_opt.edit_dir.Text:=extractfilepath(paramstr(0))+'Lists\';
Form_opt.checkbox_timer.Checked:=IniFile.ReadBool('options','checktimer',true);
Form_opt.checkbox_mus.Checked:=IniFile.ReadBool('options','checkmusic',true);
/////
Form_splash.ProgressBar1.Position:=55;
/////
Form_opt.CurrentLanguage:=IniFile.ReadString('options','language',extractfilepath(paramstr(0))+'Data\rus.lng');
if form_opt.ChangeLanguage(Form_opt.CurrentLanguage)=false
  then
  begin
  if Form_opt.CurrentLanguage<>extractfilepath(paramstr(0))+'Data\rus.lng'
          then Application.MessageBox(pchar(ErrorMessage1+#13+Form_opt.CurrentLanguage),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
  Form_opt.CurrentLanguage:=extractfilepath(paramstr(0))+'Data\rus.lng';
  if form_opt.ChangeLanguage(Form_opt.CurrentLanguage)=false
    then
    begin
    form_opt.CreateLanguage;
    form_opt.ChangeLanguage(Form_opt.CurrentLanguage);
    end;
  end;
/////
Form_splash.ProgressBar1.Position:=60;
/////
Form_opt.CheckBox1.Checked:=IniFile.ReadBool('options','autoload',false);
Form_opt.edit1.Text:=IniFile.ReadString('options','autofile','');
if Form_opt.Edit1.text=''
  then Form_opt.CheckBox1.Checked:=false;
if (not fileExists(Form_opt.edit1.Text))and Form_opt.CheckBox1.Checked then
  begin
  Application.MessageBox(pchar(ErrorMessage1+#13+Form_opt.edit1.Text),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
//  Form_opt.edit1.Text:='';
  Form_opt.CheckBox1.Checked:=false;
  end;
/////
Form_splash.ProgressBar1.Position:=65;
/////
reset(t);
Form_max1.RadioButton_go.Checked:=true;
Form_max2.RadioButton_go.Checked:=true;
combobox_prog.Clear;
Form_max2.combobox_prog.Clear;
while not eof(t) do
  begin
  readln(t,s);
10:
  if s='[closer programs]' then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 10;
      combobox_prog.Items.Add(s);
      end;
    end;
  if s='[timer programs]' then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 10;
      Form_max2.combobox_prog.Items.Add(s);
      end;
    
    end;
  end;
CloseFile(t);
/////
Form_splash.ProgressBar1.Position:=75;
/////
if FilePath<>CloserFile then
  begin
  reset(t);
  Form_max1.RadioButton_capt.Checked:=true;
  Form_max2.RadioButton_capt.Checked:=true;
  combobox_prog.Clear;
  Form_max2.combobox_prog.Clear;
  while not eof(t) do
    begin
    readln(t,s);
20:
    if s='[closer captions]' then
      begin
      while not eof(t) do
        begin
        readln(t,s);
        if (s='')or(s[1]='[')
          then goto 20;
        combobox_prog.Items.Add(s);
        end;
      end;
    if s='[timer captions]' then
      begin
      while not eof(t) do
        begin
        readln(t,s);
        if (s='')or(s[1]='[')
          then goto 20;
        Form_max2.combobox_prog.Items.Add(s);
        end;
      end;
    end;
CloseFile(t);
/////
Form_splash.ProgressBar1.Position:=85;
/////
reset(t);
Form_max1.RadioButton_file.Checked:=true;
Form_max2.RadioButton_file.Checked:=true;
combobox_prog.Clear;
Form_max2.combobox_prog.Clear;
while not eof(t) do
  begin
  readln(t,s);
30:
  if s='[closer files]' then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 30;
      combobox_prog.Items.Add(s);
      end;
    end;
  if s='[timer files]' then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 30;
      Form_max2.combobox_prog.Items.Add(s);
      end;
    
    end;
  end;
  CloseFile(t);
  Form_max1.RadioButton_go.Checked:=true;
  Form_max2.RadioButton_go.Checked:=true;
  end;
/////
Form_splash.ProgressBar1.Position:=95;
/////
if n15.Checked
  then
  begin
  Form_main.Width:=width;
  Form_main.Height:=Height;
  N15.ImageIndex:=19;
  end
  else
  begin
  Form_main.Width:=443;
  Form_main.Height:=237;
  N15.ImageIndex:=-1;
  end;
if N17.Checked
  then N17.ImageIndex:=19
  else N17.ImageIndex:=-1;
if N18.Checked
  then N18.ImageIndex:=19
  else N18.ImageIndex:=-1;
if N31.Checked
  then N31.ImageIndex:=19
  else N31.ImageIndex:=-1;
/////
Form_splash.ProgressBar1.Position:=100;
/////
form_max1.left:=Form_main.Left+Form_main.Width;
form_max1.Top:=Form_main.Top+53;
form_max2.Left:=Form_main.Left;
form_max2.Top:=Form_main.Top+Form_main.Height;
///////
Form_splash.close;
end;

procedure TForm_main.WriteIniFile(FilePath: string);
label 10,20;
const TempFile='c:\temp.txt';
var s:string;
    t,temp,origin:TextFile;
    IniFile:TIniFile;
    i:integer;
begin
IniFile:=TIniFile.Create(FilePath);
assignfile(t,FilePath);
assignfile(temp,TempFile);
assignfile(origin,CloserFile);

reset(origin);
rewrite(temp);
while not eof(origin) do
  begin
  readln(origin,s);
  writeln(temp,s);
  end;
closefile(origin);
closefile(temp);
Erase(t);
RenameFile(TempFile,FilePath);
IniFile.WriteBool('program','maxwindow1',ShowRdBtns);
IniFile.WriteBool('program','maxwindow2',showTimer);
IniFile.WriteBool('program','savesize',n15.Checked);
IniFile.WriteBool('program','savepos',n16.Checked);
IniFile.WriteBool('program','autominimize',N17.Checked);
IniFile.WriteBool('program','startminimize',N18.Checked);
IniFile.WriteBool('program','autodelete',N31.Checked);
if n15.Checked
    then
    begin
    IniFile.WriteInteger('program','width',Form_main.Width);
    IniFile.WriteInteger('program','height',Form_main.height);
    end;
if n16.Checked
    then
    begin
    IniFile.WriteInteger('program','left',Form_main.left);
    IniFile.WriteInteger('program','top',Form_main.top);
    end;

IniFile.WriteString('options','music',Form_opt.edit_bud.Text);
IniFile.WriteString('options','message',Form_opt.edit_soob.Text);
IniFile.WriteString('options','mmusic',Form_opt.edit_soob_mus.Text);
IniFile.WriteString('options','directory',Form_opt.edit_dir.Text);
IniFile.WriteInteger('options','timer',Form_opt.spinedit_timer.Value);
IniFile.WriteBool('options','checkmusic',Form_opt.checkbox_mus.Checked);
IniFile.WriteBool('options','checktimer',Form_opt.checkbox_timer.Checked);
IniFile.WriteBool('options','autoload',Form_opt.checkbox1.Checked);
IniFile.WriteString('options','autofile',Form_opt.edit1.Text);

IniFile.WriteString('options','scheme',Form_opt.CurrentScheme);
IniFile.WriteString('options','language',Form_opt.CurrentLanguage);

reset(t);
rewrite(temp);
while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  if ((form_max1.prog)and(s='[closer programs]'))
      or ((form_max1.capt)and(s='[closer captions]'))
      or ((form_max1.file1)and(s='[closer files]')) then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 10;
      end;
    end;
  end;
10:
closefile(temp);
append(temp);
for i:=0 to combobox_prog.Items.Count-1 do
  begin
  writeln(temp,combobox_prog.Items[i]);
  end;
writeln(temp,s);
while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  end;
closefile(temp);
closeFile(t);
Erase(t);
RenameFile(TempFile,FilePath);

reset(t);
rewrite(temp);
while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  if ((form_max2.prog1)and(s='[timer programs]'))
      or ((form_max2.capt1)and(s='[timer captions]'))
      or ((form_max2.file11)and(s='[timer files]')) then
    begin
    while not eof(t) do
      begin
      readln(t,s);
      if (s='')or(s[1]='[')
        then goto 20;
      end;
    end;
  end;
20:
closefile(temp);
append(temp);
for i:=0 to form_max2.combobox_prog.Items.Count-1 do
  begin
  writeln(temp,form_max2.combobox_prog.Items[i]);
  end;
writeln(temp,s);
while not eof(t) do
  begin
  readln(t,s);
  writeln(temp,s);
  end;
closefile(temp);
closeFile(t);
Erase(t);
RenameFile(TempFile,FilePath);
end;

procedure TForm_main.ReadXXCFile(FilePath: string);
var t:textfile;
    s:string;
begin
if FilePath<>''then
  begin
  listbox_programs.Items.Clear;
  form_max2.listbox_prog.Items.Clear;
  assignfile(t,FilePath);
  reset(t);
  while not eof(t) do
    begin
    readln(t,s);
    if s='[main]' then
      while not eof(t) do
        begin
        readln(t,s);
        if s= '[/main]' then break;
        listbox_programs.Items.Add(s);
        end;
    if s='[timer]' then
      while not eof(t) do
        begin
        readln(t,s);
        if s= '[/timer]' then break;
        form_max2.listbox_prog.Items.Add(s);
        end;
    end;
  closefile(t);
  form_max2.Timer1.Enabled:=true;

  Form_max2.b:=TBitMap.Create;
  Form_max2.num:=0;
  Form_max2.ImgLst_clock.GetBitmap(Form_max2.num,Form_max2.b);
  Form_main.BitBtn4.Glyph:=Form_max2.b;

  CheckHorizontalBar(Form_main.listbox_programs);
  CheckHorizontalBar(form_max2.listbox_prog);
  end;
end;

procedure TForm_main.WriteXXCFile(FilePath: string);
var i:integer;
    t:textfile;
begin
assignfile(t,FilePath);
rewrite(t);
writeln(t,'[main]');
for i:=0 to listbox_programs.Items.Count-1 do
  begin
  writeln(t,listbox_programs.Items[i]);
  end;
writeln(t,'[/main]');
writeln(t,'[timer]');
for i:=0 to form_max2.listbox_prog.Items.Count-1 do
  begin
  writeln(t,form_max2.listbox_prog.Items[i]);
  end;
writeln(t,'[/timer]');
closefile(t);
end;

procedure TForm_main.N110Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(sec);
if sec>=60 then
  begin
  sec:=0;
  inc(min);
  if min>=60 then
    begin
    min:=0;
    inc(hour);
    if hour>=24 then
      begin
      hour:=0;
      end;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.N52Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(min);
if min>=60 then
  begin
  min:=0;
  inc(hour);
  if hour>=24 then
    begin
    hour:=0;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.N111Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(MaskEdit_time.Text,7,2));
min:=strtoint(copy(MaskEdit_time.Text,4,2));
hour:=strtoint(copy(MaskEdit_time.Text,1,2));
inc(hour);
if hour>=24 then
  begin
  hour:=0;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
MaskEdit_time.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.N114Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(Form_max2.MaskEdit.Text,7,2));
min:=strtoint(copy(Form_max2.MaskEdit.Text,4,2));
hour:=strtoint(copy(Form_max2.MaskEdit.Text,1,2));
inc(sec);
if sec>=60 then
  begin
  sec:=0;
  inc(min);
  if min>=60 then
    begin
    min:=0;
    inc(hour);
    if hour>=24 then
      begin
      hour:=0;
      end;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
Form_max2.MaskEdit.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.N113Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(Form_max2.MaskEdit.Text,7,2));
min:=strtoint(copy(Form_max2.MaskEdit.Text,4,2));
hour:=strtoint(copy(Form_max2.MaskEdit.Text,1,2));
inc(min);
if min>=60 then
  begin
  min:=0;
  inc(hour);
  if hour>=24 then
    begin
    hour:=0;
    end;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
Form_max2.MaskEdit.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.N112Click(Sender: TObject);
var sec,min,hour:integer;
    ssec,smin,shour:string;
begin
sec:=strtoint(copy(Form_max2.MaskEdit.Text,7,2));
min:=strtoint(copy(Form_max2.MaskEdit.Text,4,2));
hour:=strtoint(copy(Form_max2.MaskEdit.Text,1,2));
inc(hour);
  if hour>=24 then
  begin
  hour:=0;
  end;
if length(inttostr(sec))<>1
  then ssec:=inttostr(sec)
  else ssec:='0'+inttostr(sec);
if length(inttostr(min))<>1
  then smin:=inttostr(min)
  else smin:='0'+inttostr(min);
if length(inttostr(hour))<>1
  then shour:=inttostr(hour)
  else shour:='0'+inttostr(hour);
Form_max2.MaskEdit.Text:=concat(shour,':',smin,':',ssec);
end;

procedure TForm_main.BitBtn4Click(Sender: TObject);
begin
if not ShowTimer
  then
    begin
    form_max2.Visible:=true;
    end;

timer3.Enabled:=true;
combobox_prog.SetFocus;
end;

procedure TForm_main.BitBtn2Click(Sender: TObject);
begin
ListBox_programs.SetFocus;
listbox_programs.items.delete(listbox_programs.itemindex);
CheckHorizontalBar(Form_main.listbox_programs);
end;

procedure TForm_main.BitBtn1Click(Sender: TObject);
var s:string;
    i,m:integer;
begin
m:=0;
if (ComboBox_prog.Text<>strAlarm)and(ComboBox_prog.Text<>strMessage)
          and(ComboBox_prog.Text<>strShut)and(ComboBox_prog.Text<>strReboot)
  then
    for i:=1 to length(ComboBox_prog.Text) do
      begin
      if ComboBox_prog.Text[i]='"'
        then m:=m+1;
      end;

if ComboBox_prog.text<>''
  then
  if ((pos('.exe',ComboBox_prog.Text)>4)and(ComboBox_prog.Text[length(ComboBox_prog.Text)]='e')and(ComboBox_prog.Text[length(ComboBox_prog.Text)-1]='x')and(ComboBox_prog.Text[length(ComboBox_prog.Text)-2]='e')
          and(ComboBox_prog.Text[length(ComboBox_prog.Text)-3]='.')and(pos(':\',ComboBox_prog.Text)=2)
          and((Form_max1.radiobutton_kill.checked)or(Form_max1.radiobutton_go.checked)))or
          (ComboBox_prog.Text=strAlarm)or(ComboBox_prog.Text=strMessage)or(ComboBox_prog.Text=strShut)or(ComboBox_prog.Text=strReboot)
          or((pos('"',ComboBox_prog.Text)=1)and (m>=2)and(ComboBox_prog.Text[length(ComboBox_prog.Text)]='"')
          or((form_max1.RadioButton_file.checked)and(pos(':\',ComboBox_prog.Text)=2))and(ComboBox_prog.Text[length(ComboBox_prog.Text)-3]='.'))
    then
    begin
    //����
    if (pos('<',ComboBox_prog.text)=0) and (pos(':\',ComboBox_prog.text)<>0)
              and (Form_max1.radiobutton_file.Checked)
      then
      begin
      if maskedit_time.Text[1]='0'
        then
        begin
        listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                    copy(maskedit_time.Text,2,7)+'  (F)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
        end
        else
        begin
        listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'  (F)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
        end;

      s:=combobox_prog.text;
      for i:=0 to (combobox_prog.items.count-1) do
        begin
        if s=combobox_prog.items[i]
          then combobox_prog.items.Delete(i);
        end;
      combobox_prog.items.Insert(0,s);
      combobox_prog.text:=s;
      if combobox_prog.items.count=11
        then combobox_prog.items.Delete(10);
      end;

    //���������
    if (pos('<',ComboBox_prog.text)=0) and (pos(':\',ComboBox_prog.text)<>0) and(pos('.exe',ComboBox_prog.text)<>0)
              and (not Form_max1.radiobutton_capt.Checked) and (not Form_max1.radiobutton_class.Checked)
      then
        begin
        if maskedit_time.Text[1]='0'
          then
            begin
        if Form_max1.radiobutton_kill.Checked
          then listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                      copy(maskedit_time.Text,2,7)+'  (x)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
        if Form_max1.radiobutton_go.Checked
          then listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                      copy(maskedit_time.Text,2,7)+'  (+)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
            end
          else
            begin
            if Form_max1.radiobutton_kill.Checked
              then listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'  (x)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
            if Form_max1.radiobutton_go.Checked
              then listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'  (+)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');
            end;

        s:=combobox_prog.text;
        for i:=0 to (combobox_prog.items.count-1) do
          begin
          if s=combobox_prog.items[i]
            then combobox_prog.items.Delete(i);
          end;
        combobox_prog.items.Insert(0,s);
        combobox_prog.text:=s;
        if combobox_prog.items.count=11
          then combobox_prog.items.Delete(10);
        end;

    if (ComboBox_prog.Text=strShut)or(ComboBox_prog.Text=strMessage)
          or(ComboBox_prog.Text=strAlarm)
          or(ComboBox_prog.Text=strReboot)
      then
        begin
        if maskedit_time.Text[1]='0'
          then
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                      copy(MaskEdit_time.text,2,7)+'       '+ComboBox_prog.Text)
          else
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'       '+ComboBox_prog.Text);
        combobox_prog.Text:='';
        end;

    if (Form_max1.radiobutton_capt.Checked)and(ComboBox_prog.text<>strMessage)//���������
                  and(ComboBox_prog.text<>strReboot)
                  and(ComboBox_prog.text<>strShut)
                  and(ComboBox_prog.text<>strAlarm)
      then
        begin
        if maskedit_time.Text[1]='0'
          then
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                      copy(MaskEdit_time.text,2,7)+'  (=)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')')
          else
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'  (=)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');

        s:=combobox_prog.text;
        for i:=0 to (combobox_prog.items.count-1) do
          begin
          if s=combobox_prog.items[i]
            then combobox_prog.items.Delete(i);
          end;
        combobox_prog.items.Insert(0,s);
        combobox_prog.text:=s;
        if combobox_prog.items.count=11
          then combobox_prog.items.Delete(10);
        end;

    if (Form_max1.radiobutton_class.Checked)and(ComboBox_prog.text<>strMessage) //�����
                  and(ComboBox_prog.text<>strReboot)
                  and(ComboBox_prog.text<>strShut)
                  and(ComboBox_prog.text<>strAlarm)
      then
        begin
        if maskedit_time.Text[1]='0'
          then
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'    '+
                      copy(MaskEdit_time.text,2,7)+'  (#)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')')
          else
            listbox_programs.items.add(datetostr(datetimepicker_date.date)+'   '+
                      MaskEdit_time.text+'  (#)  '+ComboBox_prog.Text+'  (*'+inttostr(Form_max1.SpinEdit_val.Value)+')');

        s:=combobox_prog.text;
        for i:=0 to (combobox_prog.items.count-1) do
          begin
          if s=combobox_prog.items[i]
            then combobox_prog.items.Delete(i);
          end;
        combobox_prog.items.Insert(0,s);
        combobox_prog.text:=s;
        if combobox_prog.items.count=11
          then combobox_prog.items.Delete(10);
        end;
    form_max1.SpinEdit_val.Value:=1;
    CheckHorizontalBar(Form_main.listbox_programs);
    end
    else
    begin
    Application.MessageBox(pchar(ErrorMessage2),pchar(Form_main.Caption),MB_ICONERROR+mb_Ok);
    end
  else combobox_prog.Text:='';
combobox_prog.SetFocus;
end;

procedure TForm_main.BitBtn3Click(Sender: TObject);
var s:string;
begin
if listbox_programs.Items.Count<>0
  then
    begin
    form_edit.DateTimePicker_date.Date:=strtodate(copy(listbox_programs.items[listbox_programs.itemindex],1,11));
    if copy(listbox_programs.Items[listbox_programs.itemindex],14,1)=' '
      then form_edit.MaskEdit_time.text:='0'+copy(listbox_programs.items[listbox_programs.itemindex],15,7)
      else form_edit.MaskEdit_time.text:=copy(listbox_programs.items[listbox_programs.itemindex],14,8);
    form_edit.edit_time_temp.text:=form_edit.MaskEdit_time.text;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='(F)'
          then
          begin
          form_edit.prog2:=false;
          form_edit.capt2:=false;
          form_edit.file2:=true;
          form_edit.RadioButton_file.checked:=true;
          form_edit.Edit_programs.CharCase:=eclowercase;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
          form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
          end;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='(+)'
          then
          begin
          form_edit.prog2:=true;
          form_edit.capt2:=false;
          form_edit.file2:=false;
          form_edit.RadioButton_go.checked:=true;
          form_edit.Edit_programs.CharCase:=eclowercase;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
          form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
          end;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='(x)'
          then
          begin
          form_edit.prog2:=true;
          form_edit.capt2:=false;
          form_edit.file2:=false;
          form_edit.RadioButton_kill.checked:=true;
          form_edit.Edit_programs.CharCase:=eclowercase;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
          form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
          end;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='(=)'
          then
          begin
          form_edit.prog2:=false;
          form_edit.capt2:=true;
          form_edit.file2:=false;
          form_edit.RadioButton_capt.checked:=true;
          form_edit.Edit_programs.CharCase:=ecnormal;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
          form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
          end;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='(#)'
          then
          begin
          form_edit.prog2:=false;
          form_edit.capt2:=true;
          form_edit.file2:=false;
          form_edit.RadioButton_class.checked:=true;
          form_edit.Edit_programs.CharCase:=ecnormal;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
          form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
          end;

    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='   '
          then
          begin
          form_edit.prog2:=false;
          form_edit.capt2:=false;
          form_edit.file2:=false;
          form_edit.Edit_programs.CharCase:=eclowercase;
          form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28);
          end;
    if copy(listbox_programs.items[listbox_programs.itemindex],24,3)='***'
          then
          begin
          if (pos('<',listbox_programs.items[listbox_programs.Itemindex])=0) and   //����
                (pos('"',listbox_programs.items[listbox_programs.Itemindex])=0)
            then
            begin
            form_edit.prog2:=true;
            form_edit.capt2:=false;
            form_edit.file2:=false;
            form_edit.RadioButton_go.checked:=true;
            form_edit.Edit_programs.CharCase:=eclowercase;
            form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
            form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
            end;
          if (pos('<',listbox_programs.items[listbox_programs.Itemindex])=0) and   //���������
                (pos('"',listbox_programs.items[listbox_programs.Itemindex])=0)
                and(pos('.exe',listbox_programs.items[listbox_programs.Itemindex])<>0)
            then
            begin
            form_edit.prog2:=true;
            form_edit.capt2:=false;
            form_edit.file2:=false;
            form_edit.RadioButton_go.checked:=true;
            form_edit.Edit_programs.CharCase:=eclowercase;
            form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
            form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
            end;
          if (pos('<',listbox_programs.items[listbox_programs.Itemindex])<>0) and   //����
                (pos('"',listbox_programs.items[listbox_programs.Itemindex])=0)
            then
            begin
            form_edit.prog2:=false;
            form_edit.capt2:=false;
            form_edit.file2:=false;
            form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28);
            end;
          if pos('"',listbox_programs.items[listbox_programs.Itemindex])<>0    //���������
            then
            begin
            form_edit.prog2:=false;
            form_edit.capt2:=true;
            form_edit.file2:=false;
            form_edit.RadioButton_capt.checked:=true;
            form_edit.edit_programs.text:=copy(listbox_programs.items[listbox_programs.itemindex],29,length(listbox_programs.items[listbox_programs.itemindex])-28-6);
            form_edit.SpinEdit_val.Value:=strtoint(copy(listbox_programs.items[listbox_programs.itemindex],length(listbox_programs.items[listbox_programs.itemindex])-1,1));
            end;

          end;

    s:=listbox_programs.items[listbox_programs.itemindex];
    listbox_programs.items.Delete(listbox_programs.itemindex);
    if form_edit.showmodal=mrcancel
      then listbox_programs.items.add(s);
    end;
CheckHorizontalBar(Form_main.listbox_programs);
listbox_programs.SetFocus;
end;

procedure TForm_main.BitBtn5Click(Sender: TObject);
begin
if (form_max1.prog or form_max1.capt) and(opendialog_addexe.execute)
  then
    begin
    if not form_max1.RadioButton_kill.checked
      then form_max1.RadioButton_go.Checked:=true;
    form_max1.prog:=true;
    form_max1.capt:=false;
    form_max1.file1:=false;
    ComboBox_prog.text:=opendialog_addexe.filename;
    end;
if form_max1.file1 and(opendialog_addfile.execute)
  then
    begin
    ComboBox_prog.text:=opendialog_addfile.filename;
    end;
ComboBox_prog.SetFocus;
end;

procedure TForm_main.BitBtn6Click(Sender: TObject);
begin
if not ShowRdBtns
  then
    begin
    form_max1.Visible:=true;
    end;

timer2.Enabled:=true;
combobox_prog.SetFocus;
end;

procedure TForm_main.BitBtn3MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_button3;
end;

procedure TForm_main.BitBtn2MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_button2;
end;

procedure TForm_main.BitBtn1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_button1;
end;

procedure TForm_main.BitBtn5MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_button5;
end;

procedure TForm_main.BitBtn6MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:='';
end;

procedure TForm_main.BitBtn4MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
statusbar1.panels[0].text:=tray_button4;
end;

end.
