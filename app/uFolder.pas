unit uFolder;          //����� �����

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ShellCtrls, Grids, {Outline,} DirOutln,
  FileCtrl;

type
  TForm_dir = class(TForm)
    Button_ok: TButton;
    Button_close: TButton;
    ShellTreeView1: TShellTreeView;
    procedure Button_closeClick(Sender: TObject);
    procedure Button_okClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_dir: TForm_dir;

implementation

uses uOptions;

{$R *.dfm}

procedure TForm_dir.Button_closeClick(Sender: TObject);
begin
close;
end;

procedure TForm_dir.Button_okClick(Sender: TObject);
begin
form_opt.edit_dir.text:=shellTreeView1.Path;
close;
end;

procedure TForm_dir.FormResize(Sender: TObject);
begin
if Form_dir.height<270
  then Form_dir.height:=270;
if Form_dir.Width<177
  then Form_dir.width:=177;
ShellTreeView1.Width:=Form_dir.Width-17;
ShellTreeView1.Height:=Form_dir.Height-75;
button_ok.Top:=Form_dir.Height-65;
button_ok.Left:=Form_dir.Width-158;
button_close.top:=Form_dir.Height-65;
button_close.Left:=Form_dir.Width-83;
end;

procedure TForm_dir.FormShow(Sender: TObject);
begin
Form_dir.Left:=form_opt.Left+20;
Form_dir.Top:=form_opt.top+10;
end;

end.
